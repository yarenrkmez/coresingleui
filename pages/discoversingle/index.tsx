import { DiscoverMain, ContainerMainPage, Post } from "@components";

import * as React from "react";

interface Props {}

const Index: React.FunctionComponent<Props> = () => {
  return (
    <ContainerMainPage
      filtersSideStatus={false}
      rightstyle="hidden lg:flex "
      leftstyle="hidden sm:flex"
      childrenStyle="overflow-y-auto "
      leftroomstyle="hidden lg:flex"
    >
      <label className="font-aller p-2 text-1375">#Chronic Headache</label>
      <Post />
      <Post />
      <Post />
    </ContainerMainPage>
  );
};

export default Index;
