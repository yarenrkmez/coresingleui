import React from "react";
import Image from "next/image";
import {
  ContainerBlue,
  ContainerWhite,
  ContainerForm,
  Input,
  ButtonPrimary,
  Polygon,
  FieldInput,
} from "@components";
import { Formik, Form } from "formik";
import * as Yup from "yup";
const Forgot: React.FC = () => {
  const lowercaseRegex = /(?=.*[a-z])/;
  const uppercaseRegex = /(?=.*[A-Z])/;
  const numericRegex = /(?=.*[0-9])/;

  const newPasswordSchema = Yup.object().shape({
    password: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required")
      .matches(lowercaseRegex, "one lowercase required!")
      .matches(uppercaseRegex, "one uppercase required!")
      .matches(numericRegex, "one number required!"),
    confirmpassword: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required")
      .oneOf([Yup.ref("password")], "Password must be the same!"),
    termsOfUse: Yup.bool().isTrue(
      "I agree to the Terms Of Use must be checked"
    ),
  });
  return (
    <div className="flex h-screen  w-full  ">
      <ContainerWhite style="justify-center">
        <Formik
          initialValues={{
            username: "",
            password: "",
            name: "",
            surname: "",
            email: "",
            confirmPassword: "",
          }}
          validationSchema={newPasswordSchema}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          <Form>
            <ContainerForm>
              <div className="flex flex-start  justify-start  ">
                <Image
                  src="/icons/asdas.svg"
                  alt="nextjs"
                  width="40px"
                  height="40px"
                />
              </div>

              <label className=" text font-bold font-aller">
                Type your new password to sign in
              </label>
              <div className="flex flex-col">
                <label className="inputlabel">Password</label>
                <FieldInput
                  name="password"
                  type="password"
                  placeholder="Password"
                />
              </div>

              <div className="flex flex-col ">
                <label className="inputlabel  ">Confirm Password</label>
                <FieldInput
                  name="confirmpassword"
                  type="password"
                  placeholder="Confirm Password"
                />
              </div>

              <div className="flex ">
                <ButtonPrimary
                  title="Reset Password"
                  style="bg-secondary-cyan w-90px text-forgot lg:w-150px font-inter   lg:text-buttontext text-primary-white "
                />
              </div>

              <div className="flex justify-center  items-center p-3 ">
                <div className="h-0 border w-full border-others-softText bg-others-softText"></div>
              </div>

              <label className="text-center  rememberlabel p-2">
                Already have an account?{" "}
                <label className="text-secondary-cyan   rememberlabel font-bold ">
                  Log in
                </label>
              </label>
            </ContainerForm>
          </Form>
        </Formik>
        <Polygon style="absolute" />
      </ContainerWhite>
      <ContainerBlue style="bg-secondary-turquoise">
        <div className=" flex flex-col content-between space-y-16  items-center justify-center w-394px">
          <Image
            src="/icons/darkmatehand.svg"
            alt="nextjs"
            width="60px"
            height="60px"
          />
          <label className="font-bold text-center  xs:text-forgot  sm:text-sm lg:text-buttontext font-aller xl:text-1375  text-primary-black">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor
            duis sollicitudin at nulla.
          </label>
          <ButtonPrimary
            title="Why Join Us"
            style="bg-primary-white text-secondary-cyan border-secondary-cyan border-2 w-215px font-inter p-2 text-md"
          />
        </div>
      </ContainerBlue>
    </div>
  );
};
export default Forgot;
