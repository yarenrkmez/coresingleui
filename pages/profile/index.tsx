import { ContainerMainPage, UserProfile } from "@components";

import * as React from "react";

interface Props {}

const Index: React.FunctionComponent<Props> = (props) => {
  return (
    <ContainerMainPage
      childrenStyle="overflow-y-auto"
      leftstyle="hidden sm:flex"
      rightstyle="hidden lg:flex"
      filtersSideStatus={false}
    >
      <UserProfile />
    </ContainerMainPage>
  );
};

export default Index;
