import { Settings, ContainerMainPage } from "@components";

import * as React from "react";

type ISettingsProps = {};

const Index: React.FunctionComponent<ISettingsProps> = (props) => {
  return (
    <ContainerMainPage
      doctorsSideStatus={false}
      discoverSideStatus={false}
      filtersSideStatus={false}
      childrenStyle="sm:ml-3 "
      leftstyle="hidden sm:flex"
      leftroomstyle="hidden lg:flex"
      rightstyle="hidden lg:flex"
    >
      <Settings />
    </ContainerMainPage>
  );
};

export default Index;
