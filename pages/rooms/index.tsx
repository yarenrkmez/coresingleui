import { ContainerMainPage, RoomsMain } from "@components";

import * as React from "react";

interface Props {}

const Index: React.FunctionComponent<Props> = (props) => {
  return (
    <ContainerMainPage
      roomsSideStatus={false}
      filtersSideStatus={false}
      childrenStyle="overflow-y-auto"
      leftstyle="hidden sm:flex "
      rightstyle="hidden lg:flex "
    >
      <RoomsMain />
    </ContainerMainPage>
  );
};

export default Index;
