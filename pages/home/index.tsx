import { Post, NewPost, ContainerMainPage } from "@components";

import * as React from "react";

interface IHomeProps {}

const Home: React.FunctionComponent<IHomeProps> = (props) => {
  return (
    <ContainerMainPage
      filtersSideStatus={false}
      rightstyle="hidden lg:flex "
      leftstyle="hidden sm:flex"
      childrenStyle="overflow-y-auto "
      leftroomstyle="hidden lg:flex"
    >
      <NewPost title="Create new post" inChat={false} />
      <Post />
      <Post />
      <Post />
      <Post />
      <Post />
      <Post />
    </ContainerMainPage>
  );
};

export default Home;
