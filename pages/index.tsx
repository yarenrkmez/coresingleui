import React, { useState } from "react";
import Image from "next/image";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { setUserSession } from "../src/redux/actions/userActions";

import {
  ContainerBlue,
  ContainerWhite,
  ContainerForm,
  Input,
  ButtonPrimary,
  ButtonSecondary,
  Polygon,
  FieldInput,
} from "@components";
import { RootState } from "@redux/reducers/rootReducer";

const Home: React.FC = () => {
  const lowercaseRegex = /(?=.*[a-z])/;
  const uppercaseRegex = /(?=.*[A-Z])/;
  const numericRegex = /(?=.*[0-9])/;

  const dispatch = useDispatch();
  const SignupSchema = Yup.object().shape({
    password: Yup.string()
      .min(8, "Password must be more than 8 characters!")
      .max(16, "Password must be less than 16 characters!")
      .required("Required")
      .matches(lowercaseRegex, "one lowercase required!")
      .matches(uppercaseRegex, "one uppercase required!")
      .matches(numericRegex, "one number required!"),
    username: Yup.string()
      .min(8, "Username must be more than 8 characters!")
      .max(16, "Username must be less than 16 characters!")
      .required("Required"),
  });
  const { user } = useSelector((state: RootState) => state.userReducer);
  console.log(user);
  return (
    <div className="flex  h-screen w-full justify-center ">
      <ContainerWhite>
        <ContainerForm>
          <div className="flex flex-start justify-start items-center ">
            <Image
              src="/icons/asdas.svg"
              alt="nextjs"
              width="40px"
              height="40px"
            />
          </div>

          <label className=" text font-bold font-aller">
            Find your illness mate and share your experiences.
          </label>
          <Formik
            initialValues={{
              username: "",
              password: "",
            }}
            validationSchema={SignupSchema}
            onSubmit={(values) => {
              console.log(values);
              dispatch(setUserSession(values));
            }}
            validateOnChange
          >
            <Form className="space-y-2">
              <div className="flex flex-col">
                <label className="  inputlabel  ">Username</label>
                <FieldInput
                  label="username"
                  name="username"
                  type="username"
                  placeholder="Username"
                />
              </div>

              <div className="flex flex-col ">
                <label className=" inputlabel ">Password</label>
                <FieldInput
                  label="password"
                  name="password"
                  type="password"
                  placeholder="Password"
                />
              </div>

              <div className="flex justify-start items-center">
                <input type="checkbox" />
                <label className="ml-2  rememberlabel">Remember me</label>
              </div>
              <div className="flex justify-between  items-center">
                <ButtonPrimary
                  title="Log in"
                  style="bg-secondary-cyan w-90px font-inter  text-primary-white  text-buttontext p-2"
                />
                <label className="font-bold baselabel ">Forgot Password?</label>
              </div>
              <div className="flex justify-center items-center">
                <span className="flex border w-3/5 xl:w-4/5  border-others-softText bg-primary-twgray"></span>
                <label className="  w-full font-interregular text-center xs:text-xs sm:text-sm  text-others-twGray lg:text-tiny ">
                  Or continue with
                </label>
                <span className="flex border w-3/5 xl:w-4/5  border-others-softText bg-primary-twgray"></span>
              </div>
              <div className="flex justify-evenly p-3">
                <ButtonSecondary style="w-50px">
                  <Image
                    src="/icons/google.svg"
                    alt="nextjs"
                    width="14px"
                    height="14px"
                  />
                </ButtonSecondary>
                <ButtonSecondary style="w-50px">
                  <Image
                    src="/icons/facebook.svg"
                    alt="nextjs"
                    width="14px"
                    height="14px"
                  />
                </ButtonSecondary>
                <ButtonSecondary style="w-50px">
                  <Image
                    src="/icons/twitter.svg"
                    alt="nextjs"
                    width="14px"
                    height="14px"
                  />
                </ButtonSecondary>
              </div>
              <div className=" justify-center text-center">
                <label className="text-center   rememberlabel ">
                  Need an account?{" "}
                  <label className="text-secondary-cyan  rememberlabel  font-bold ">
                    Create an Account
                  </label>
                </label>
              </div>
            </Form>
          </Formik>
        </ContainerForm>
        <Polygon style="absolute" />
      </ContainerWhite>
      <ContainerBlue style="bg-secondary-cyan">
        <div className=" flex flex-col content-between space-y-16 items-center justify-center  md:w-300px lg:w-394px">
          <Image
            src="/icons/matehandwhite.svg"
            alt="nextjs"
            width="60px"
            height="60px"
          />

          <label className=" text-center  font-aller  md:text-lg lg:text-1375  text-primary-white">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor
            duis sollicitudin at nulla.
          </label>
          <ButtonPrimary
            title="Why Join Us ?"
            style="bg-secondary-turquoise w-215px font-inter p-2 text-md text-primary-white "
          />
        </div>
      </ContainerBlue>
    </div>
  );
};
export default Home;
