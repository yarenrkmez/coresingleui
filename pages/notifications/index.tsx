import { Notifications, ContainerMainPage } from "@components";

import * as React from "react";

interface Props {}

const Index: React.FunctionComponent<Props> = (props) => {
  return (
    <ContainerMainPage
      filtersSideStatus={false}
      childrenStyle="overflow-y-auto"
      leftstyle="hidden sm:flex"
      rightstyle="hidden lg:flex"
    >
      <Notifications />
    </ContainerMainPage>
  );
};

export default Index;
