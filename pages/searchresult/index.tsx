import {
  DoctorsMain,
  RoomsMain,
  Post,
  ContainerMainPage,
  Users,
} from "@components";

import * as React from "react";

type ISearchResultProps = {};

const Index: React.FunctionComponent<ISearchResultProps> = (props) => {
  return (
    <ContainerMainPage
      roomsSideStatus={false}
      doctorsSideStatus={false}
      childrenStyle="overflow-y-auto"
      leftstyle="hidden sm:flex"
      rightstyle="hidden lg:flex"
    >
      <Users />
      <DoctorsMain buttonstyle="flex" usertextstyle="flex flex-col" />
      <RoomsMain buttonstyle="flex" />
      <Post />
    </ContainerMainPage>
  );
};

export default Index;
