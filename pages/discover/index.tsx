import { DiscoverMain, ContainerMainPage } from "@components";

import * as React from "react";

interface Props {}

const Index: React.FunctionComponent<Props> = () => {
  return (
    <ContainerMainPage
      discoverSideStatus={false}
      filtersSideStatus={false}
      leftstyle="hidden sm:flex"
      rightstyle="hidden lg:flex"
      childrenStyle="overflow-y-auto"
    >
      <DiscoverMain />
    </ContainerMainPage>
  );
};

export default Index;
