import { ContainerMainPage, Messages } from "@components";

import * as React from "react";
import useWindowSize from "../../src/utils/windowSize"
interface Props { }

const Index: React.FunctionComponent<Props> = (props) => {
  const { width } = useWindowSize();

  return (
    <ContainerMainPage
      discoverSideStatus={false}
      doctorsSideStatus={false}
      filtersSideStatus={false}
      roomsSideStatus={width > 1024 ? true : false}
      leftstyle="hidden lg:flex"
      rightstyle="hidden lg:flex"
    >
      <Messages />
    </ContainerMainPage>
  );
};

export default Index;
