import React from "react";
import Image from "next/image";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  ContainerBlue,
  ContainerWhite,
  ContainerForm,
  Input,
  ButtonPrimary,
  Polygon,
  FieldInput,
} from "@components";

const SignUp: React.FC = () => {
  const lowercaseRegex = /(?=.*[a-z])/;
  const uppercaseRegex = /(?=.*[A-Z])/;
  const numericRegex = /(?=.*[0-9])/;

  const SignupSchema = Yup.object().shape({
    name: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required"),
    surname: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required"),
    username: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required"),
    email: Yup.string().email().required(),
    password: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required")
      .matches(lowercaseRegex, "one lowercase required!")
      .matches(uppercaseRegex, "one uppercase required!")
      .matches(numericRegex, "one number required!"),
    confirmPassword: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required")
      .oneOf([Yup.ref("password")], "Password must be the same!"),
    termsOfUse: Yup.bool().isTrue(
      "I agree to the Terms Of Use must be checked"
    ),
  });
  return (
    <div className="flex    ">
      <ContainerWhite style="justify-between md:justify-end overflow-y-auto min-h-screen">
        <Formik
          initialValues={{
            username: "",
            password: "",
            name: "",
            surname: "",
            email: "",
            confirmPassword: "",
          }}
          validationSchema={SignupSchema}
          onSubmit={(values) => {
            console.log(values);
          }}
        >
          <Form>
            <ContainerForm>
              <div className="flex flex-start  justify-start ">
                <Image
                  src="/icons/asdas.svg"
                  alt="nextjs"
                  width="40px"
                  height="40px"
                />
              </div>

              <label className="text font-bold font-aller">
                Find your illness mate and share your experiences.
              </label>
              <div className="flex flex-col">
                <label className="inputlabel">Name</label>
                <FieldInput name="name" type="name" placeholder="John" />
              </div>

              <div className="flex flex-col">
                <label className="inputlabel">Surname</label>
                <FieldInput name="surname" type="surname" placeholder="Doe" />
              </div>

              <div className="flex flex-col ">
                <label className="inputlabel">City</label>
                <Input Placeholder="Adana" />
              </div>

              <div className="flex flex-col">
                <label className="inputlabel">Username</label>
                <FieldInput
                  name="username"
                  type="username"
                  placeholder="Username"
                />
              </div>

              <div className="flex flex-col">
                <label className="inputlabel">E-mail</label>
                <FieldInput
                  name="email"
                  type="email"
                  placeholder="john_doe@mailinator.com"
                />
              </div>

              <div className="flex flex-col">
                <label className="inputlabel">Password</label>
                <FieldInput
                  name="password"
                  type="password"
                  placeholder="******"
                />
              </div>

              <div className="flex flex-col">
                <label className="inputlabel">Confirm Password</label>
                <FieldInput
                  name="confirmPassword"
                  type="confirmPassword"
                  placeholder="******"
                />
              </div>

              <div className="flex justify-start items-center">
                <input name="termsOfUse" type="checkbox" />
                <label className="ml-2 rememberlabel">
                  I agree to the{" "}
                  <label className="font-bold rememberlabel">
                    Terms Of Use
                  </label>
                </label>
              </div>

              <ButtonPrimary
                title="Register"
                style="bg-secondary-cyan w-90px font-inter  text-buttontext p-2 text-primary-white"
              />

              <div className="flex justify-center  items-center p-3 ">
                <div className="h-0 border w-full border-others-softText"></div>
              </div>

              <label className="text-center rememberlabel">
                Already have an account?{" "}
                <label className="text-secondary-cyan rememberlabel font-bold ">
                  Log in
                </label>
              </label>
            </ContainerForm>
          </Form>
        </Formik>
        <div className="flex bottom-0  w-full self-end items-center ">
          <Polygon />
        </div>
      </ContainerWhite>

      <ContainerBlue style="lg:bg-secondary-cyan bg-secondary-turquoise   ">
        <div className="flex flex-col content-between space-y-12 items-center  justify-center w-394px">
          <div className=" lg:hidden">
            <Image
              src="/icons/darkmatehand.svg"
              alt="nextjs"
              width="60px"
              height="60px"
            />
          </div>

          <div className="hidden lg:flex">
            <Image
              src="/icons/matehandwhite.svg"
              alt="nextjs"
              width="60px"
              height="60px"
            />
          </div>

          <label className="text-center  font-aller  md:text-lg lg:text-1375  text-primary-white">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor
            duis sollicitudin at nulla.
          </label>
          <ButtonPrimary
            title="Why Join Us ?"
            style="md:bg-primary-white lg:bg-secondary-turquoise md:text-secondary-cyan md:border font-inter md:border-secondary-cyan lg:text-primary-white font-bold font-inter w-215px "
          />
        </div>
      </ContainerBlue>
    </div>
  );
};
export default SignUp;
