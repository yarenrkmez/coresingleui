import React from "react";
import Image from "next/image";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import {
  ContainerBlue,
  ContainerWhite,
  ContainerForm,
  Input,
  ButtonPrimary,
  Polygon,
  FieldInput,
} from "@components";

const Forgot: React.FC = () => {
  const lowercaseRegex = /(?=.*[a-z])/;
  const uppercaseRegex = /(?=.*[A-Z])/;
  const numericRegex = /(?=.*[0-9])/;

  const ForgotSchema = Yup.object().shape({
    username: Yup.string()
      .min(8, "Too Short!")
      .max(16, "Too Long!")
      .required("Required"),
  });

  return (
    <div className="flex h-screen   justify-center ">
      <ContainerWhite style="justify-center">
        <ContainerForm>
          <div className="flex flex-start justify-start items-center ">
            <Image
              src="/icons/asdas.svg"
              alt="nextjs"
              width="40px"
              height="40px"
            />
          </div>

          <label className=" text font-bold font-aller">
            Type your username or e-mail to reset password
          </label>
          <Formik
            initialValues={{
              username: "",
              password: "",
            }}
            validationSchema={ForgotSchema}
            onSubmit={(values) => {
              console.log(values);
            }}
            validateOnChange
          >
            <Form className="space-y-2">
              <div className="flex flex-col ">
                <label className="inputlabel  ">Username/Email</label>
                <FieldInput
                  label="username"
                  name="username"
                  type="username"
                  placeholder="Username"
                />
              </div>

              <div className="flex ">
                <ButtonPrimary
                  title="Reset Password"
                  style="bg-secondary-cyan w-90px text-forgot lg:w-150px font-inter  lg:text-buttontext text-primary-white "
                />
              </div>
              <div className="flex justify-center  items-center p-3 ">
                <div className="h-0 border w-full border-others-softText bg-others-softText"></div>
              </div>
              <div className=" justify-center text-center">
                <label className="text-center  rememberlabel p-2">
                  Already have an account?{" "}
                  <label className="text-secondary-cyan   rememberlabel font-bold ">
                    Log in
                  </label>
                </label>
              </div>
            </Form>
          </Formik>
        </ContainerForm>

        <Polygon style="absolute" />
      </ContainerWhite>
      <ContainerBlue style="bg-secondary-turquoise">
        <div className=" flex flex-col content-between space-y-16  items-center justify-center w-394px">
          <Image
            src="/icons/darkmatehand.svg"
            alt="nextjs"
            width="60px"
            height="60px"
          />
          <label className="font-bold text-center  xs:text-forgot  sm:text-sm lg:text-buttontext font-aller xl:text-1375  text-primary-black">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Porttitor
            duis sollicitudin at nulla.
          </label>
          <ButtonPrimary
            title="Why Join Us"
            style="bg-primary-white  text-secondary-cyan border-secondary-cyan border-2 w-215px font-inter p-2 text-md"
          />
        </div>
      </ContainerBlue>
    </div>
  );
};
export default Forgot;
