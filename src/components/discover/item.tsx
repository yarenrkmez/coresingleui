import React from "react";

type Props = {
  title?: string;
  subtitle?: string;
};

const Item: React.FC<Props> = ({ title, subtitle }) => {
  return (
    <div className="flex flex-col">
      <label className="inline-block truncate align-text-baseline mr-0.5 text-loginWhyJoinUs text-primary-black font-inter">
        {title}
      </label>

      <label className=" text-postCategory truncate text-others-twGray font-interregular text-buttontext ">
        {subtitle}
      </label>
    </div>
  );
};

export default Item;
