import React from "react";
import Item from "./item";
import { UilAngleRight } from "@iconscout/react-unicons";
import { Icons, ButtonLink, ButtonIcon } from "@components";
type Props = {
  style?: String;
};

export const Index: React.FC<Props> = ({ style }) => {
  return (
    <div
      className={`${style} flex-start flex-col mb-3  shadow-lg space-y-5 md:min-w-208 p-6 rounded-2xl bg-primary-white`}
    >
      <p className="font truncate font-bold text-loginRightSize font-aller text-1375 text-primary-black">
        Discover
      </p>
      <Item title="#Covid-19" subtitle="16,6 K Posts" />
      <Item title="#BionTech" subtitle="5.616 Posts" />
      <Item title="diet plan" subtitle="3.206 Posts" />
      <Item title="#Chronic Headache" subtitle="1.582 K Posts" />
      <Item title="#nutrition" subtitle="2.109 K Posts" />
      <Item title="#foods_health" subtitle="1.109 K Posts" />

      <ButtonLink
        Icon={UilAngleRight}
        style="text-primary-twgray hidden md:flex text-sm font-interregular"
        title="See More"
        IconStyle=" font-light text-center"
      />
    </div>
  );
};
