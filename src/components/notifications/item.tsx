import React from "react";
import { Icons } from "@components";

import {
  UilCommentMessage,
  UilHeart,
  UilHeartBreak,
  UilRedo,
  UilEllipsisH,
} from "@iconscout/react-unicons";
import { ButtonPrimary, ButtonSecondary } from "@components/button";
import { NotifItemButtonStatus } from "../../enums/notifications";

interface Props {
  buttonStatus?: number;
}

const Item: React.FC<Props> = ({ buttonStatus }) => {
  return (
    <div className="flex justify-between items-center  ">
      <div className="flex flex-col">
        <div className="flex flex-row ">
          <img
            src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
            alt="user"
            className="userImagePost"
          />

          <div className="flex flex-col">
            <div className="flex items-center">
              <div className=" flex justify-start">
                <div className="drPolygon bg-primary-white absolute">
                  <label className="drText  text-secondary-cyan p-1">DR</label>
                </div>
                <svg
                  width="20"
                  height="18"
                  viewBox="0 0 20 18"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                  className="relative"
                >
                  <path
                    d="M1.14434 7.75C0.697756 8.5235 0.697756 9.4765 1.14434 10.25L4.48964 16.0442C4.93622 16.8177 5.76154 17.2942 6.6547 17.2942H13.3453C14.2385 17.2942 15.0638 16.8177 15.5104 16.0442L18.8557 10.25C19.3022 9.4765 19.3022 8.5235 18.8557 7.75L15.5104 1.95577C15.0638 1.18227 14.2385 0.705772 13.3453 0.705772H6.6547C5.76154 0.705772 4.93622 1.18227 4.48964 1.95577L1.14434 7.75Z"
                    stroke="#00AFF1"
                  />
                </svg>
              </div>
              <label className="  font-interregular text-md ml-2 ">
                yarenilal
              </label>
              <label className="font-inter text-md ml-2">followed you</label>
              <label className="font-interregular text-buttontext ml-2 text-primary-twgray">
                22m
              </label>
            </div>
            <label className="text-buttontext font-interregular text-primary-twgray">
              @ilal
            </label>
          </div>
        </div>
      </div>
      {buttonStatus === NotifItemButtonStatus.FollowButton && (
        <ButtonSecondary
          title="Follow"
          style="text-secondary-cyan hidden lg:flex justify-center items-center w-70px p-1 text-buttontext  font-inter"
        />
      )}
      {buttonStatus === NotifItemButtonStatus.UnFollowButton && (
        <ButtonSecondary
          title="Unfollow"
          style="text-primary-white hidden lg:flex justify-center items-center w-70px  p-1 text-buttontext  font-inter bg-secondary-cyan"
        />
      )}
    </div>
  );
};

export default Item;
