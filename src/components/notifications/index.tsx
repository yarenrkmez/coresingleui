import React from "react";
import Item from "./item";

interface Props {}

export const Index: React.FC<Props> = (props: Props) => {
  return (
    <div className="flex flex-col p-7 space-y-7 shadow-md rounded-xl bg-primary-white">
      <label className="text-1375 text-primary-black font-aller">
        Notifications
      </label>
      <Item buttonStatus={1} />
      <Item buttonStatus={2} />
      <Item buttonStatus={3} />
      <Item buttonStatus={1} />
      <Item buttonStatus={2} />
      <Item buttonStatus={3} />
    </div>
  );
};
