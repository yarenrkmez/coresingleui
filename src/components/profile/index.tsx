import React, { useState } from "react";
import { UilAngleRight } from "@iconscout/react-unicons";
import { ButtonLink } from "@components";
import { ProfileHeader } from "./profileHeader";
import { Post } from "@components/cards/Post";
import { ProfileSection } from "src/enums/profile";
import { PostsSection } from "./postsSection";
import { LikesSection } from "./likesSection";
import { SharesSection } from "./sharesSection";
import { CommentsSection } from "./commentsSection";

type Props = {};

export const Index: React.FC<Props> = (props: Props) => {
  const [chosenSection, setChosenSection] = useState(
    ProfileSection.PostsSection
  );
  return (
    <div className="flex flex-col">
      <div className="flex flex-col bg-primary-white rounded-xl h-full">
        <ProfileHeader />
      </div>
      <div className="flex flex-row pt-8 pb-8  justify-around text-primary-black ">
        <button
          className="profileSection"
          onClick={() => setChosenSection(ProfileSection.PostsSection)}
        >
          {chosenSection === ProfileSection.PostsSection ? (
            <label className="font-aller text-1375">Gönderiler</label>
          ) : (
            <label className="font-allerregular text-lg">Gönderiler</label>
          )}
        </button>
        <button
          className="profileSection"
          onClick={() => setChosenSection(ProfileSection.CommentsSection)}
        >
          {chosenSection === ProfileSection.CommentsSection ? (
            <label className="font-aller text-1375">Yorumlar</label>
          ) : (
            <label className="font-allerregular text-lg">Yorumlar</label>
          )}
        </button>
        <button
          className="profileSection"
          onClick={() => setChosenSection(ProfileSection.LikesSection)}
        >
          {chosenSection === ProfileSection.LikesSection ? (
            <label className="font-aller text-1375">Beğeniler</label>
          ) : (
            <label className="font-allerregular text-lg">Beğeniler</label>
          )}
        </button>
        <button
          className="profileSection"
          onClick={() => setChosenSection(ProfileSection.SharesSection)}
        >
          {chosenSection === ProfileSection.SharesSection ? (
            <label className="font-aller text-1375">Paylaşımlar</label>
          ) : (
            <label className="font-allerregular text-lg">Paylaşımlar</label>
          )}
        </button>
      </div>
      {chosenSection === ProfileSection.PostsSection && <PostsSection />}
      {chosenSection === ProfileSection.CommentsSection && <CommentsSection />}
      {chosenSection === ProfileSection.LikesSection && <LikesSection />}
      {chosenSection === ProfileSection.SharesSection && <SharesSection />}
    </div>
  );
};
