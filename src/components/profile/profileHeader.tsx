import React, { useState } from "react";

import { UilMapMarker, UilPen } from "@iconscout/react-unicons";
import Image from "next/image";

import { ButtonLink } from "@components";
import { Icons } from "@components/icons";
import { UserCard } from "@components/cards/UserCard";
import { EditProfile } from "@components/popups/editProfile";

interface Props {}

export const ProfileHeader: React.FC<Props> = (props: Props) => {
  const [editProfileVisibilty, setEditProfileVisibility] = useState(false);
  const callbackFunction = (childata: any) => {
    setEditProfileVisibility(childata);
  };
  return (
    <div>
      {editProfileVisibilty && (
        <div className="flex justify-center">
          <EditProfile callback={callbackFunction} />
        </div>
      )}
      <div className="flex flex-row relative">
        <Image
          src="/icons/userHead.svg"
          alt="userhead"
          width="1029px"
          height="406px"
          className="border rounded-xl border-b-0 rounded-b-none"
        />
        <div className="absolute -bottom-9 ml-5">
          <div className="flex  absolute object-cover  ">
            <img
              src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
              alt="user"
              className="userAvatarPolygon"
            />
          </div>

          <svg
            width="82"
            height="72"
            viewBox="0 0 82 72"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.01036 33.25C1.02788 34.9517 1.02788 37.0483 2.01036 38.75L19.1236 68.391C20.1061 70.0927 21.9218 71.141 23.8867 71.141H58.1132C60.0782 71.141 61.8939 70.0927 62.8764 68.391L79.9896 38.75C80.9721 37.0483 80.9721 34.9517 79.9896 33.25L62.8764 3.60898C61.8939 1.90727 60.0782 0.858978 58.1132 0.858978H23.8868C21.9218 0.858978 20.1061 1.90727 19.1236 3.60898L2.01036 33.25Z"
              stroke="#53E0DB"
            />
          </svg>
        </div>
      </div>

      <div className="flex flex-col p-5 shadow-md border-b-0 rounded-b-none rounded-xl bg-primary-white  "></div>

      <div className="flex flex-col  pl-7 pr-7 border-t-0 rounded-t-none shadow-md rounded-xl  bg-primary-white ">
        <div
          className="flex justify-end"
          onClick={() => setEditProfileVisibility(true)}
        >
          <Icons Icon={UilPen} />
        </div>

        <div className="flex relative flex-row pt-4 space-x-3 items-center">
          <div>
            <div className="drPolygon bg-primary-white absolute">
              <label className="drText  p-1">DR</label>
            </div>
            <svg
              width="20"
              height="18"
              viewBox="0 0 20 18"
              fill="none"
              xmlns="http://www.w3.org/2000/svg"
              className="relative"
            >
              <path
                d="M1.14434 7.75C0.697756 8.5235 0.697756 9.4765 1.14434 10.25L4.48964 16.0442C4.93622 16.8177 5.76154 17.2942 6.6547 17.2942H13.3453C14.2385 17.2942 15.0638 16.8177 15.5104 16.0442L18.8557 10.25C19.3022 9.4765 19.3022 8.5235 18.8557 7.75L15.5104 1.95577C15.0638 1.18227 14.2385 0.705772 13.3453 0.705772H6.6547C5.76154 0.705772 4.93622 1.18227 4.48964 1.95577L1.14434 7.75Z"
                stroke="#00AFF1"
              />
            </svg>
          </div>

          <label className="font-aller sm:text-sm xl:text-1375 font-bold text-primary-black">
            Mehmet Oz
          </label>

          <label className="text-primary-twgray font-interregular text-tiny font-light">
            @mehmetOz
          </label>
          <div className="flex flex-row space-x-2 text-primary-black text-linkButtontext">
            <Icons Icon={UilMapMarker} />
            <label className="text-primary-black font-interregular text-tiny font-light">
              Bern / Switzerland
            </label>
          </div>
        </div>
        <p className="font-interregular  text-tiny pb-2">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc, lacinia
          enim iaculis massa scelerisque sagittis nisl semper. Auctor praesent
          donec enim aenean a ac, habitant eget. At cras aenean ut volutpat,
          convallis vitae.
        </p>
        <div className="flex flex-row text-linkButtontext text-primary-black pb-7">
          <div className="flex flex-row">
            <label className="font-inter text-md mr-1">2K </label>
            <label className="font-interregular text-tiny mr-2">Takipçi</label>
          </div>
          <div className="flex flex-row">
            <label className="font-inter text-md mr-1">215</label>
            <label className="font-interregular text-tiny mr-2">
              Takip Edilen
            </label>
          </div>
          <div className="flex flex-row">
            <label className="font-inter text-md mr-1">25</label>
            <label className="font-interregular text-tiny mr-2">
              Takip edilen oda
            </label>
          </div>
        </div>
      </div>
    </div>
  );
};
