import React, { useState } from 'react'
import {
    UilAngleRight,

} from "@iconscout/react-unicons";
import { ButtonLink } from '@components'
import { ProfileHeader } from './profileHeader';
import { Post } from '@components/cards/Post';

type Props = {

}

export const PostsSection: React.FC<Props> = (props: Props) => {
    return (
        <div className="flex flex-col">
           <Post />
           <Post />
           <Post />
           <Post />
        </div>

    )
}

