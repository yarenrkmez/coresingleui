import React from "react";

type IButtonSecondaryProps = {
  title?: string;
  children?: any;
  style?: string;
};

type IButtonIconProps = {
  Icon?: any;
  style?: string;
  onClick?:any
};
type IButtonLinkProps = {
  Icon?: any;
  style?: string;
  title?: string;
  IconStyle?: string;
};

export const ButtonPrimary: React.FC<IButtonSecondaryProps> = ({
  children,
  title,
  style,
}) => {
  return (
    <button
      className={`  flex  rounded-full max-w-lg min-w-md h-auto items-center justify-center p-2.5   ${style}`}
    >
      {children} {title}
    </button>
  );
};

export const ButtonSecondary: React.FC<IButtonSecondaryProps> = ({
  children,
  title,
  style,
}) => {
  return (
    <button
      className={`${style} bg-primary-white border  min-w-sm   border-secondary-cyan rounded-full`}
    >
      {children} {title}
    </button>
  );
};

export const ButtonIcon: React.FC<IButtonIconProps> = ({ Icon, style,onClick }) => {
  return (
    <button className={`${style} m-2`} onClick={()=>onClick()} >
      <Icon />
    </button>
  );
};

export const ButtonLink: React.FC<IButtonLinkProps> = ({
  Icon,
  style,
  title,
  IconStyle,
}) => {
  return (
    <button className={`${style} text-secondary-cyan items-center`}>
      {title}
      <Icon className={`${IconStyle} `} />
    </button>
  );
};
