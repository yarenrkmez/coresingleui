import React from "react";
import Item from "./item";
import { UilAngleRight } from "@iconscout/react-unicons";
import { ButtonLink } from "@components";
type Props = {};

export const Index: React.FC<Props> = (props: Props) => {
  return (
    <div className="flex flex-col relative flex-start space-y-5 lg:min-w-308  shadow-lg  mb-3  rounded-2xl p-6  bg-primary-white">
      <div className="font-aller text-1375">Users</div>
      <Item buttonStatus={2} title={"Jessica Neil"} />
      <Item buttonStatus={2} title={"Adis Coco"} />
      <Item buttonStatus={2} title={"Jessica Neil"} />
      <Item buttonStatus={1} title={"Jessica Neil"} />
      <Item buttonStatus={1} title={"Adis Coco"} />
      <Item buttonStatus={1} title={"Jessica Neil"} />

      <ButtonLink
        Icon={UilAngleRight}
        style="p-2 text-primary-twgray hidden md:flex text-sm font-interregular"
        title="See More"
        IconStyle=" font-light text-center"
      />
    </div>
  );
};
