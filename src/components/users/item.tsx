import React from "react";
import { UserCard, ButtonSecondary } from "@components";
import { ItemButtonStatus } from "../../enums/rooms";
type Props = {
  buttonStatus?: number;
  title?: string;
};

const Item: React.FC<Props> = ({ buttonStatus, title }) => {
  return (
    <div className="flex justify-between items-center">
      <UserCard
        user={{
          name: `${title}`,
          surname: "Matehand",
          userName: "Matehand",
          userImage: "",
        }}
        usertextstyle="flex flex-col"
      />

      {buttonStatus === ItemButtonStatus.FollowButton && (
        <ButtonSecondary
          title="Follow"
          style="text-secondary-cyan justify-center items-center w-70px  p-1 text-buttontext  font-inter"
        />
      )}
      {buttonStatus === ItemButtonStatus.UnFollowButton && (
        <ButtonSecondary
          title="Unfollow"
          style="text-primary-white  justify-center items-center w-70px  p-1 text-buttontext  font-inter bg-secondary-cyan"
        />
      )}
    </div>
  );
};

export default Item;
