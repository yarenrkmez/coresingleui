import React from "react";

import { UilAngleRight } from "@iconscout/react-unicons";
import { Icons } from "@components";
import { ButtonPrimary, ButtonSecondary, ButtonLink } from "@components/button";
import { ItemButtonStatus } from "../../enums/rooms";

type Props = {
  buttonStatus?: number;
  title?: string;
  numberOfPosts?: string;
  buttonstyle?: string;
};

const Item: React.FC<Props> = ({
  buttonStatus,
  title,
  numberOfPosts,
  buttonstyle,
}) => {
  return (
    <div className=" flex    justify-between items-center ">
      <div className="flex flex-col">
        <div>
          <label className=" w-full font-inter text-md mt-4 ">{title}</label>
        </div>
        <label className=" text-buttontext font-aller text-primary-twgray ">
          {numberOfPosts} K Posts
        </label>
      </div>

      {buttonStatus === ItemButtonStatus.FollowButton && (
        <ButtonSecondary
          title="Follow"
          style={`${buttonstyle} text-secondary-cyan  justify-center items-center w-70px  p-1 text-buttontext  font-inter`}
        />
      )}
      {buttonStatus === ItemButtonStatus.UnFollowButton && (
        <ButtonSecondary
          title="Unfollow"
          style={`${buttonstyle} text-primary-white  justify-center items-center w-70px  p-1 text-buttontext  font-inter bg-secondary-cyan`}
        />
      )}
    </div>
  );
};

export default Item;
