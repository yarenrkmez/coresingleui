import React from "react";
import Item from "./item";
import { UilAngleRight } from "@iconscout/react-unicons";
import { ButtonLink } from "@components";

type Props = {
  style?: string;
  buttonstyle?: string;
};

export const Index: React.FC<Props> = ({ style, buttonstyle }) => {
  return (
    <div
      className={`${style} flex-start flex-col mb-3  shadow-lg space-y-5 md:min-w-208 p-6 rounded-2xl bg-primary-white`}
    >
      <label className="font-aller text-1375">Rooms</label>
      <Item
        buttonStatus={1}
        title="Acil"
        numberOfPosts="16.6"
        buttonstyle={`${buttonstyle}`}
      />
      <Item
        buttonStatus={2}
        title="Coronavirus"
        numberOfPosts="13.6"
        buttonstyle={`${buttonstyle}`}
      />
      <Item
        buttonStatus={2}
        title="Alerji"
        numberOfPosts="6.6"
        buttonstyle={`${buttonstyle}`}
      />
      <Item
        buttonStatus={1}
        title="Migren"
        numberOfPosts="6.2"
        buttonstyle={`${buttonstyle}`}
      />
      <Item
        buttonStatus={2}
        title="D Vitamini Eksikliği"
        numberOfPosts="3.6"
        buttonstyle={`${buttonstyle}`}
      />
      <Item
        buttonStatus={1}
        title="Uyku Bozuklukları"
        numberOfPosts="2.6"
        buttonstyle={`${buttonstyle}`}
      />

      <ButtonLink
        Icon={UilAngleRight}
        style="text-primary-twgray hidden md:flex text-sm font-interregular"
        title="See More"
        IconStyle=" font-light text-center"
      />
    </div>
  );
};
