import * as React from "react";

interface IInputProps {
  Placeholder?: string;
  className?:string;
}

export const Input: React.FunctionComponent<IInputProps> = ({
  Placeholder,
  className,
}) => {
  return (
    <input
      placeholder={Placeholder}
      className={`border border-others-softText rounded-3xl  text-xs lg:text-sm max-w-sm p-2 font-interregular placeholder-primary-black  ${className} `}
    />
  );
};
