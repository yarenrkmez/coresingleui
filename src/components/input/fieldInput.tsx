import * as React from "react";
import { Formik, Form, Field, ErrorMessage, useField } from "formik";

type Fieldprops = {
  placeholder?: string;
  className?: string;
  name?: string;
  type?: string;
  label?: string;
};

export const FieldInput: React.FunctionComponent<Fieldprops> = ({
  placeholder,
  className,
  type,
  label,
  ...props
}) => {
  const [field, meta] = useField(props);

  return (
    <>
      {meta.touched ? (
        <>
          {meta.error ? (
            <input
              type={type}
              placeholder={placeholder}
              className={`${
                meta.error && "is-invalid"
              } border border-secondary-red outline-none rounded-3xl  text-xs lg:text-sm max-w-sm p-2 font-interregular placeholder-primary-black  ${className}  `}
              {...field}
              {...props}
            />
          ) : (
            <>
              <input
                type={type}
                placeholder={placeholder}
                className={`${
                  meta.error && "is-invalid"
                } border border-feeling-green outline-none rounded-3xl  text-xs lg:text-sm max-w-sm p-2 font-interregular placeholder-primary-black  ${className}  `}
                {...field}
                {...props}
              />
            </>
          )}
        </>
      ) : (
        <input
          type={type}
          placeholder={placeholder}
          className={`${
            meta.touched && meta.error && "is-invalid"
          } border border-primary-twgray outline-none  clickbackground  rounded-3xl  text-xs lg:text-sm max-w-sm p-2 font-interregular placeholder-primary-black  ${className}  `}
          {...field}
          {...props}
        />
      )}

      <ErrorMessage
        component="div"
        name={field.name}
        className="error text-secondary-red ml-2 font-interregular text-buttontext "
      />
    </>
  );
};
