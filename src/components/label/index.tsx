import * as React from "react";

interface IAppProps {
  title?: string;
}

export const Label: React.FC<IAppProps> = ({ title }) => {
  return (
    <div className="hidden sm:flex sm:text-sm md:text-md lg:text-md xl:text-md ml-3 font-interregular ">
      {title}
    </div>
  );
};

export const RoomsHeaderLabel: React.FC<IAppProps> = ({ title }) => {
  return <div className=" w-full font-inter text-md mt-4 ">{title}</div>;
};

export const RoomsFooterLabel: React.FC<IAppProps> = ({ title }) => {
  return (
    <div className=" text-buttontext font-aller text-primary-twgray ">
      {title}
    </div>
  );
};

export const PostPopupLabel: React.FC<IAppProps> = ({ title }) => {
  return <div className=" min-w-min  text-md  hover:font-thin">{title}</div>;
};
