import * as React from "react";

import { Icons } from "@components";
import { Label } from "@components";
import { useRouter } from "next/router";
type IItemProps = {
  Icon?: any;
  Title?: string;
};

export const Item: React.FunctionComponent<IItemProps> = ({ Icon, Title }) => {
  const router = useRouter();
  const { pathname } = router;

  return (
    <div
      className=" flex justify-start hoverbackground  p-4"
      onClick={() => router.push(`/${Title}`.toLowerCase())}
    >
      {pathname === `/${Title}`.toLowerCase() ? (
        <div className=" flex justify-start  text-secondary-cyan">
          <Icons Icon={Icon} />
          <Label title={Title} />
        </div>
      ) : (
        <div className=" flex justify-start ">
          <Icons Icon={Icon} />
          <Label title={Title} />
        </div>
      )}
    </div>
  );
};
