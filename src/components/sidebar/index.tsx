import React from "react";
import {
  UilEstate,
  UilTelescope,
  UilApps,
  UilBell,
  UilCommentMessage,
  UilUser,
  UilCog,
} from "@iconscout/react-unicons";

import { Item } from "./item";
type Props = {
  style?: String;
};
export const Index: React.FC<Props> = ({ style }) => {
  return (
    <div
      className={`${style} flex flex-start flex-col shadow-lg mb-3  md:min-w-208   rounded-2xl p-6 bg-primary-white `}
    >
      <Item Icon={UilEstate} Title="Home" />

      <Item Icon={UilTelescope} Title="Discover" />

      <Item Icon={UilApps} Title="Rooms" />

      <Item Icon={UilBell} Title="Notifications" />

      <Item Icon={UilCommentMessage} Title="Messages" />

      <Item Icon={UilUser} Title="Profile" />

      <Item Icon={UilCog} Title="Settings" />
    </div>
  );
};
