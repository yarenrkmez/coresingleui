import * as React from "react";

type Props = {
  style?: string;
};

export const Polygon: React.FC<Props> = ({ style }) => {
  return (
    <div className={`${style} bottom-0 left-0    w-full `}>
      <svg
        width="140"
        viewBox="0 0 170 212"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
        className=" w-80px sm:w-100px md:w-110px lg:w-120px xl:w-140px "
      >
        <path
          d="M41.9666 34.3587C45.6569 33.0074 49.7878 33.7275 52.8032 36.2477L162.943 128.301C165.959 130.821 167.4 134.759 166.726 138.63L142.075 280.041C141.4 283.912 138.711 287.13 135.021 288.481L0.230592 337.838C-3.4597 339.19 -7.59055 338.47 -10.606 335.949L-120.746 243.896C-123.761 241.376 -125.203 237.439 -124.528 233.567L-99.8779 92.1564C-99.2031 88.2849 -96.514 85.0675 -92.8237 83.7162L41.9666 34.3587Z"
          stroke="#00CC83"
          strokeWidth="8"
          strokeLinejoin="round"
        />
        <path
          d="M36.5893 65.2067C40.2796 63.8554 44.4105 64.5755 47.4259 67.0957L133.539 139.068C136.555 141.588 137.997 145.526 137.322 149.397L118.049 259.96C117.374 263.831 114.685 267.049 110.995 268.4L5.60799 306.991C1.91772 308.342 -2.21318 307.622 -5.2286 305.102L-91.3421 233.129C-94.3575 230.609 -95.7994 226.672 -95.1245 222.8L-75.8515 112.237C-75.1766 108.366 -72.4875 105.148 -68.7972 103.797L36.5893 65.2067Z"
          stroke="#53E0DB"
          strokeWidth="8"
          strokeLinejoin="round"
        />
      </svg>
    </div>
  );
};
export const TopPolygon: React.FunctionComponent = () => {
  return (
    <div className="absolute top-0 right-0 z-0   ">
      <svg
        width="100"
        viewBox="0 0 156 177"
        fill="none"
        xmlns="http://www.w3.org/2000/svg"
      >
        <path
          d="M172.184 -131.347C175.853 -132.676 179.946 -131.97 182.944 -129.49L294.064 -37.5826C297.101 -35.071 298.552 -31.1227 297.867 -27.236L273.042 113.701C272.357 117.588 269.642 120.817 265.925 122.164L129.935 171.443C126.266 172.773 122.173 172.067 119.175 169.587L8.0551 77.6794C5.01857 75.1679 3.56724 71.2196 4.25188 67.3328L29.0775 -73.6042C29.7622 -77.491 32.4778 -80.7204 36.194 -82.0671L172.184 -131.347Z"
          stroke="#00CC83"
          stroke-width="8"
          stroke-linejoin="round"
        />
        <path
          d="M166.764 -100.576C170.433 -101.905 174.526 -101.199 177.524 -98.7194L264.426 -26.8423C267.463 -24.3308 268.914 -20.3824 268.229 -16.4957L248.824 93.6703C248.139 97.5571 245.424 100.787 241.707 102.133L135.356 140.673C131.687 142.002 127.594 141.296 124.596 138.816L37.6935 66.9393C34.6569 64.4277 33.2056 60.4794 33.8902 56.5927L53.2957 -53.5733C53.9803 -57.4601 56.696 -60.6896 60.4121 -62.0362L166.764 -100.576Z"
          stroke="#53E0DB"
          stroke-width="8"
          stroke-linejoin="round"
        />
      </svg>
    </div>
  );
};
