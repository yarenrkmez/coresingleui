import React from "react";
import Item from "./item";

type Props = {
  style?: String;
};

export const Index: React.FC<Props> = ({ style }) => {
  return (
    <div
      className={`${style} flex flex-col flex-start space-y-5 lg:min-w-308  shadow-lg  mb-3  rounded-2xl p-6  bg-primary-white`}
    >
      <div className="font-aller text-1375">Filters</div>
      <Item buttonStatus={1} title="Users" type="checkbox" />
      <Item buttonStatus={1} title="Doctors" type="checkbox" />
      <Item buttonStatus={2} title="Rooms" type="checkbox" />
      <Item buttonStatus={1} title="Posts" type="checkbox" />

      <div className="font-aller text-md">Sort By</div>
      <Item buttonStatus={1} title="Top Match" type="radio" />
      <Item buttonStatus={1} title="Latest" type="radio" />
    </div>
  );
};
