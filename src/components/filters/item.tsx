import React from "react";
import { UserCard, ButtonSecondary } from "@components";
import { ItemButtonStatus } from "../../enums/rooms";
type Props = {
  buttonStatus?: number;
  title?: string;
  type?: string;
};

const Item: React.FC<Props> = ({ buttonStatus, title, type }) => {
  return (
    <div className="flex justify-start items-center">
      {buttonStatus === ItemButtonStatus.FollowButton && (
        <input type={`${type}`} />
      )}

      {buttonStatus === ItemButtonStatus.UnFollowButton && (
        <input type={`${type}`} />
      )}
      <label className="ml-2 font-interregular text-sm">{`${title}`}</label>
    </div>
  );
};

export default Item;
