import React from "react";
import { MessageHeader } from "./MessageHeader";
import { ChatBox } from "./ChatBox";
import { AudioPlayer } from "./AudioPlayer";
import { NewPost } from "@components/cards/NewPost";

type Props = {
    className?: string;
    callback?: any;
    goBackButton?:boolean;
    user?:{name?:string}
}

export const SingleChat: React.FC<Props> = ({ className, callback,goBackButton,user }) => {

    return (
        <div className={` flex   ${className} lg:w-full flex-col md:min-w-208 lg:min-w-308  max-w-1280 `} >
            <div className="overflow-y-auto h-full flex flex-col  pb-20 pt-8  border-l-0 rounded-l-none border-b-0 rounded-b-none shadow-md rounded-xl bg-primary-white">
                <MessageHeader onClick={() => callback(false)} goBackButton={goBackButton} user={user} />
                <div className="pl-8  pr-16 lg-Max:space-y-16 sm-Max:h-215px ">
                    <ChatBox chatBoxStyle="left" timeStyle="mt-7" />
                    <ChatBox
                        imgStyle="absolute right-0 top-full left-98%"
                        chatBoxStyle="right"
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                    <ChatBox chatBoxStyle="left" timeStyle="mt-7" />
                    <ChatBox
                        imgStyle="absolute right-0 top-full left-98%"
                        chatBoxStyle="right"
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                    <ChatBox
                        style="flex flex-col-reverse"
                        chatBoxStyle="left"
                        timeStyle="mt-7"
                    />
                    <ChatBox
                        imgStyle="absolute right-0 top-full left-98%"
                        chatBoxStyle="right"
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                    <ChatBox
                        style="flex flex-col-reverse"
                        chatBoxStyle="left"
                        timeStyle="mt-7"
                    />
                    <ChatBox
                        imgStyle="absolute right-0 top-full left-98%"
                        chatBoxStyle="right"
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                    <AudioPlayer
                        style="flex flex-col-reverse"
                        audioPlayerStyle="left"
                        isPlay={false}
                        timeStyle="mt-7"
                    />
                    <AudioPlayer
                        imageStyle="absolute right-0 top-full left-98%"
                        audioPlayerStyle="right"
                        imgStyle="absolute right-0 top-full left-98%"
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                    <ChatBox
                        style="flex flex-col-reverse"
                        chatBoxStyle="left"
                        isSendText={false}
                        isSendImg={true}
                        timeStyle="mt-7"
                        avatarAndSendTimeStyle="mb-16"
                    />
                    <ChatBox
                        imgStyle="absolute right-0 top-full left-98% "
                        chatBoxStyle="right"
                        isSendText={false}
                        isSendImg={true}
                        timeStyle="absolute right-0 right-1 top-7"
                    />
                </div>
            </div>
            <NewPost title="Send Message" />
        </div>

    );
};
