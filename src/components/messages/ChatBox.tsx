import React from "react";
import Item from "./MessagesUser";
import { UilAngleRight } from "@iconscout/react-unicons";

type Props = {
  style?: string;
  imgStyle?: string;
  chatBoxStyle?: string;
  isSendImg?: boolean;
  isSendText?: boolean;
  timeStyle?: string;
  avatarAndSendTimeStyle?: string
};

export const ChatBox: React.FC<Props> = ({
  style,
  imgStyle,
  chatBoxStyle,
  isSendImg = false,
  isSendText = true,
  timeStyle,
  avatarAndSendTimeStyle,
}) => {
  if (chatBoxStyle === "left") chatBoxStyle = "rounded-tl-lg rounded-r-lg";
  else
    chatBoxStyle =
      "absolute right-0 border rounded-tl-lg rounded-r-none messageBlue";

  return (
    <div className={` flex flex-col-reverse relative  ${style} `}>
      <div className={`flex flex-row ${avatarAndSendTimeStyle} `}>
        <img
          src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
          alt="user"
          className={`userImagePost mb-5  inline-block align-bottom ${imgStyle} `}
        />
        <p
          className={` text-others-twGray text-postCategory font-light w-11 h-4 ${timeStyle}`}
        >
          12m Ago
        </p>
      </div>

      <div
        className={`bg-primary-gray  border   max-w-208 border-primary-gray ml-10 ${chatBoxStyle}`}
      >
        {isSendImg && (
          <img
            src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
            alt="user"
            className="p-2"
          />
        )}

        {isSendText && (
          <p className="text-linkButtontext text-primary-black font-light pl-5 pr-5 pt-1 pb-1 ">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit.
          </p>
        )}
      </div>
    </div>
  );
};
