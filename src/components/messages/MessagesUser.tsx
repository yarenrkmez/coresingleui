import React, { useState } from "react";

import { UilPen } from "@iconscout/react-unicons";
import { Icons } from "@components";
import SelectUserSideItem from "./SelectUserSideItem";
import { SearchBar } from "@components/cards/SearchBar";
import { SingleChat } from "./SingleChat";
import useWindowSize from "src/utils/windowSize";

type Props = {

};

const MessagesUser: React.FC<Props> = ({

}) => {
  const { width } = useWindowSize()
  const [visiblitySingleChat, setSingleChat] = useState(false)
  const [userId, setUserId] = useState(1);
  const callbackFunction = (childata: any) => {
    setSingleChat(childata);
  };

  return (
    <div className="flex flex-row lg:ml-4 w-full">
      <div className="flex flex-col shadow-md rounded-xl space-y-5 pt-7 border-r lg-Max:rounded-xl 
 lg-Max:border-primary-white rounded-r-none border-others-softText bg-primary-white 
 lg-Max:w-full max-h-1226px lg-Max:h-835px  ">
        {!visiblitySingleChat ?
          <>
            <div className="flex flex-row justify-between pl-7 pr-7   ">
              <label className="text-primary-black font-bold text-loginRightSize ">
                Messages
              </label>
              <Icons Icon={UilPen} className="text-others-twGray" />
            </div>
            <div className="pl-7 pr-7 ">
              <SearchBar
                placeholder="Search Messages"
                style="bg-primary-gray border-primary-gray"
                placeholderStyle="text-others-twGray bg-primary-gray"
                iconStyle="text-others-twGray w-4 h-4"
              />
            </div>

            <div className="overflow-y-auto flex flex-col space-y-5">
              <SelectUserSideItem callback={width < 1024 && callbackFunction} onClick={() => setUserId(1)} />
              <SelectUserSideItem callback={width < 1024 && callbackFunction} onClick={() => setUserId(2)} />
              <SelectUserSideItem callback={width < 1024 && callbackFunction} onClick={() => setUserId(3)} />
              <SelectUserSideItem callback={width < 1024 && callbackFunction} onClick={() => setUserId(4)} />
              <SelectUserSideItem callback={width < 1024 && callbackFunction} onClick={() => setUserId(5)} />
            </div>
          </>

          : <SingleChat className="h-full max-h-1226px lg-Max:h-835px  " callback={callbackFunction} goBackButton={true} />


        }

        {/* {visiblitySingleChat ?
        <SingleChat className=" " callback={callbackFunction} goBackButton={true} />
        :
        <SingleChat className="lg-Max:hidden " user={{name:userId.toString()}} />
      } */}
      </div>
      <SingleChat className="lg-Max:hidden " user={{name:userId.toString()}} />


    </div>
  );
};

export default MessagesUser;
