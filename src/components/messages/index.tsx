import React from "react";
import Item from "./MessagesUser";
import { UilAngleRight } from "@iconscout/react-unicons";
import { ButtonLink } from "@components";

import { MessageHeader } from "./MessageHeader";
import { ChatBox } from "./ChatBox";
import MessagesUser from "./MessagesUser";
import { AudioPlayer } from "./AudioPlayer";
import { NewPost } from "@components/cards/NewPost";
import { SingleChat } from "./SingleChat";
interface Props { }

export const Index: React.FC<Props> = (props: Props) => {
  return (
    <div className="flex flex-row lg:ml-4">
      <MessagesUser />

    </div>
  );
};
