import React from 'react'
import { UserAvatar } from './userAvatar';

type Props= {
    buttonStatus?: number;
    title?: string;
    numberOfPosts?: string;
    imgStyle?: string;
    callback?:any;
    onClick?:any;
}



const SelectUserSideItem: React.FC<Props> = ({ buttonStatus, title, numberOfPosts, imgStyle,callback,onClick }) => {
    return (

        <button className="group flex flex-row focus:bg-secondary-cyan pl-7 pr-7 items-center" onClick={() => {callback && callback(true);onClick()}} >
            <UserAvatar imgStyle={imgStyle} />

            <div className="flex flex-col">
                <div className="flex flex-row" >
                    <label>ilal</label>
                    <label>@yrnilal</label>
                </div>

                <p className="text-postCategory text-others-twGray text-left " >Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
            </div>

        </button>
    )
}

export default SelectUserSideItem
