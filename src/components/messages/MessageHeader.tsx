import React from 'react'
import Item from './MessagesUser'
import {
    UilAngleRight,

} from "@iconscout/react-unicons";
import { ButtonLink } from '@components'
import { Icons } from "@components";

import {
    UilAngleLeft,
    UilEllipsisH,
} from "@iconscout/react-unicons";
import { UserAvatar } from './userAvatar';
import { ButtonIcon } from '@components/button';

type Props = {
    onClick?: any;
    goBackButton?: boolean;
    user?:{name?:string}
}

export const MessageHeader: React.FC<Props> = ({ onClick, goBackButton,user }) => {
    return (
        <div className="flex z-0 justify-between w-full border-b pb-7 pl-8 pr-8  lg-Max:border-primary-white">
            <div className="flex flex-col">
                <div className="flex flex-row">
                    {goBackButton &&
                        <ButtonIcon
                            Icon={UilAngleLeft}
                            style="text-primary-twgray  lg:flex"
                            onClick={() => onClick()}

                        />
                    }

                    <UserAvatar />
                    <div className="flex flex-col" >
                        <label className="pr-1">{user?.name}</label>
                        <label>@ilal</label>

                    </div>

                </div>
            </div>

            <div className="flex flex-row text-center items-center justify-around right-0 ">
                <label className="mr-5">22m</label>
                <Icons Icon={UilEllipsisH} className="text-secondary-cyan" />
            </div>
        </div>

    )
}

