import React from 'react'
import Item from './MessagesUser'
import {
    UilAngleRight,
    UilPlay,
    UilPause

} from "@iconscout/react-unicons";
import { ButtonLink } from '@components'

import { MessageHeader } from './MessageHeader'
import { Icons } from '@components/icons';
type Props = {
    imageStyle?: string,
    audioPlayerStyle?: string,
    style?: string,
    isPlay?: boolean,
    imgStyle?: string,
    timeStyle?: string

}

export const AudioPlayer: React.FC<Props> = ({ imageStyle, audioPlayerStyle, style, isPlay = true, imgStyle, timeStyle }) => {
    if (audioPlayerStyle === 'left')
        audioPlayerStyle = 'rounded-tl-lg rounded-r-lg'
    else
        audioPlayerStyle = 'absolute right-0 border rounded-tl-lg rounded-r-none messageBlue'

    return (
        <div className={`flex flex-col-reverse relative ${style}`}  >

            <div className="flex flex-row" >
                <img
                    src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
                    alt="user"
                    className={`userImagePost mb-5  inline-block align-bottom ${imgStyle} `}
                />         <p className={` text-others-twGray text-postCategory font-light w-11 h-4 ${timeStyle}`}>12m Ago</p>


            </div>
            <div className={`flex items-center flex-row bg-primary-gray  border p-3  w-52 border-primary-gray  ml-10 ${audioPlayerStyle}`} >
                {!isPlay ? <Icons Icon={UilPlay} className="text-primary-black" />
                    : <Icons Icon={UilPause} className="text-primary-black" />}
                <div className="w-1 border rounded-full h-1 bg-secondary-cyan border-secondary-cyan" />
                <div className="w-full h-1px bg-others-twGray" />

            </div>


        </div>

    )
}






