export { Header } from "./header";
export { Logo } from "./logo";
export { Main } from "./main";

export {
  ButtonPrimary,
  ButtonSecondary,
  ButtonIcon,
  ButtonLink,
} from "./button";

export { Cards } from "./cards";
export { Footer } from "./footer";
export { Label, RoomsFooterLabel, RoomsHeaderLabel } from "./label/index";
export { Icons } from "./icons";
export { Navbar } from "./cards/Navbar";
export { NewPost } from "./cards/NewPost";
export { UserCard } from "./cards/UserCard";
export { Polygon, TopPolygon } from "./polygon";

export {
  ContainerWhite,
  ContainerBlue,
  ContainerForm,
  ContainerSidebar,
  ContainerRooms,
  ContainerMainPage,
} from "./container/index";

export { Input } from "./input";
export {FieldInput} from "./input/fieldInput"
export { Post } from "./cards/Post";
export { Index as Notifications } from "./notifications/index";
export { Index as DiscoverMain } from "./discover/index";
export { Index as RoomsMain } from "./rooms/index";
export { Index as Messages } from "./messages/index";
export { Index as Sidebar } from "./sidebar/index";
export { Index as DoctorsMain } from "./doctors/index";
export { Index as Filters } from "./filters/index";
export { Index as Users } from "./users/index";
export { Index as UserProfile } from "./profile/index";
export { Index as Settings } from "./settings/index";
export { Profile } from "./popups/profile";
export { Message } from "./popups/messages";

export { SearchResult } from "./popups/searchResult";
