import React, { useState } from "react";

import SelectUserSideItem from "../messages/SelectUserSideItem";
type Props = {
  callback?: any;
  user?: { name: string; surname: string; userName: string; userImage: string };
  title?: string;
};

export const Message: React.FC<Props> = ({ callback }) => {
  return (
    <div className="flex flex-col z-20 absolute p-2 space-y-2  bg-primary-white border-primary-white rounded-lg  w-308px max-w-308   ">
      <SelectUserSideItem />
      <SelectUserSideItem />
      <SelectUserSideItem />
      <SelectUserSideItem />
      <SelectUserSideItem />
      <SelectUserSideItem />
    </div>
  );
};
