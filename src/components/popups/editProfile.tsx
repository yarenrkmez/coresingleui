import React, { useState } from "react";
import { UilUpload, UilTimes } from "@iconscout/react-unicons";
import { Icons } from "@components/icons";
import { Input } from "@components";
import { ButtonPrimary } from "@components/button";

type Props = {
  callback?: any;
};

export const EditProfile: React.FC<Props> = ({ callback }) => {
  return (
    <div className="flex flex-col fixed z-20 overflow-auto  w-full h-full left-0 top-0  items-center editProfilePopup">
      <div className="relative flex flex-col mt-20 mb-6 w-full max-w-639 justify-self-center space-x-8 border-primary-white bg-primary-white border rounded-lg justify-start" >
      <div className="flex flex-row border-b border-others-softText pt-6 pl-8 pb-6">
        <div>
          <label className="inline-block align-middle  pr-1 font-bold text-loginWhyJoinUs text-primary-black font-interregular">
            Edit Account
          </label>
          <label className="inline-block align-middle font-interregular text-postCategory text-others-twGray">
            Change account details
          </label>
        </div>
        <button onClick={() => callback(false)}>
          <Icons Icon={UilTimes} className="absolute  right-1" />
        </button>
      </div>
      <div className="flex flex-col pr-8 pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Background Image
        </label>
        <div className="flex flex-col border-dotted border-4 border-others-softText w-full h-36 justify-center text-center items-center">
          <Icons Icon={UilUpload} className="text-secondary-cyan" />
          <label className="font-interregular font-light text-loginWhyJoinUs">
            Drag & Drop Your files or Browse
          </label>
        </div>
      </div>
      <div className="flex flex-col pr-8 pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Profile Avatar
        </label>
        <div className="flex flex-col border-dotted border-4 border-others-softText w-full h-36 justify-center text-center items-center"></div>
      </div>
      <div className="flex flex-col pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Name
        </label>
        <Input className="mr-8" />
      </div>
      <div className="flex flex-col pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Surname
        </label>
        <Input className="mr-8" />
      </div>
      <div className="flex flex-col pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Bio
        </label>
        <Input className="mr-8" />
      </div>
      <div className="flex flex-col pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          Phone
        </label>
        <Input className="mr-8" />
      </div>
      <div className="flex flex-col pt-5">
        <label className="font-interregular font-bold text-linkButtontext">
          City
        </label>
        <Input className="mr-8" />
      </div>
      <div className="flex flex-row justify-end pt-5 pb-5 pr-8">
        <ButtonPrimary
          title="Save Changes"
          style="bg-secondary-cyan w-130px font-inter  text-primary-white  text-buttontext p-1"
        />
      </div>

      </div>
    </div>
  );
};
