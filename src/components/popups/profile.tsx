import React from "react";
import { UilSignout } from "@iconscout/react-unicons";

type Props = {
  callback?: any;
  user?: { name: string; surname: string; userName: string; userImage: string };
  title?: string;
};

export const Profile: React.FC<Props> = ({ callback }) => {
  const InterLabel: React.FC<Props> = ({ title }) => {
    return (
      <label className="text-primary-twgray font-interregular text-sm">
        {title}
      </label>
    );
  };

  const AllerLabel: React.FC<Props> = ({ title }) => {
    return <label className=" font-interregular text-md">{title}</label>;
  };

  return (
    <div className="flex shadow-2xl  flex-col z-20 fixed top-0   right-6 xl:right-auto xl:ml-6    p-4 space-y-2  bg-primary-white border-primary-white rounded-lg  w-215px max-w-308   ">
      <div className="flex">
        <div className="flex z-10 userImage  objectcover  ">
          <img
            src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
            alt="user"
          />

          <svg
            width="50"
            height="44"
            viewBox="0 0 50 44"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.01036 19.25C1.02788 20.9517 1.02788 23.0483 2.01036 24.75L11.1236 40.5346C12.1061 42.2363 13.9218 43.2846 15.8868 43.2846H34.1132C36.0782 43.2846 37.8939 42.2363 38.8764 40.5346L47.9896 24.75C48.9721 23.0483 48.9721 20.9517 47.9896 19.25L38.8764 3.46539C37.8939 1.76369 36.0782 0.715393 34.1133 0.715393H15.8868C13.9218 0.715393 12.1061 1.76369 11.1236 3.46539L2.01036 19.25Z"
              stroke="#53E0DB"
            />
          </svg>
        </div>

        <div className="flex flex-col ">
          <p className="font-inter ">asdsad</p>
          <p className=" baselabel text-primary-twgray  font-interregular">
            asdasd
          </p>
        </div>
      </div>

      <InterLabel title="Profile" />
      <div className="flex w-full border border-primary-twgray"></div>

      <AllerLabel title="Account" />
      <InterLabel title="Settings" />
      <InterLabel title="Be a Sponsor" />

      <div className="flex w-full border border-primary-twgray"></div>

      <AllerLabel title="Language" />
      <InterLabel title="Türkçe" />
      <InterLabel title="English" />
      <div className="flex w-full border border-primary-twgray"></div>

      <AllerLabel title="Privacy & Security" />
      <InterLabel title="About us" />
      <InterLabel title="Terms of use" />
      <InterLabel title="Terms of service" />
      <InterLabel title="Privacy policy" />
      <InterLabel title="Share Matehand" />
      <div className="flex w-full border border-primary-twgray"></div>

      <div className="flex space-x-3 ">
        <UilSignout />
        <AllerLabel title="Sign Out" />
      </div>
    </div>
  );
};
