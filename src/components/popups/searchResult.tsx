import React from "react";
import ItemDiscover from "../discover/item";
import ItemUsers from "../users/item";
type Props = {
  callback?: any;
  user?: { name: string; surname: string; userName: string; userImage: string };
  title?: string;
};

export const SearchResult: React.FC<Props> = ({ callback }) => {
  const InterLabel: React.FC<Props> = ({ title }) => {
    return (
      <label className="text-primary-twgray font-interregular text-sm">
        {title}
      </label>
    );
  };

  const AllerLabel: React.FC<Props> = ({ title }) => {
    return <label className=" font-interregular text-md">{title}</label>;
  };

  return (
    <div className="flex flex-col absolute p-2 space-y-2  z-20 bg-primary-white border-primary-white rounded-lg  w-308px max-w-308   ">
      <label className="font font-bold text-loginRightSize font-aller text-1375 text-primary-black">
        Hashtags
      </label>

      <ItemDiscover title="#Covid-19" subtitle="16,6 K Posts" />
      <ItemDiscover title="#BionTech" subtitle="5.616 Posts" />
      <ItemDiscover title="diet plan" subtitle="3.206 Posts" />
      <ItemDiscover title="#Chronic Headache" subtitle="1.582 K Posts" />
      <ItemDiscover title="#nutrition" subtitle="2.109 K Posts" />
      <ItemDiscover title="#foods_health" subtitle="1.109 K Posts" />

      <div className="flex h-0 w-full border bg-primary-twgray border-primary-twgray"></div>

      <div className="font-aller text-1375">Users</div>
      <ItemUsers buttonStatus={2} title={"Jessica Neil"} />
      <ItemUsers buttonStatus={2} title={"Adis Coco"} />
      <ItemUsers buttonStatus={2} title={"Jessica Neil"} />
      <ItemUsers buttonStatus={1} title={"Jessica Neil"} />
      <ItemUsers buttonStatus={1} title={"Adis Coco"} />
      <ItemUsers buttonStatus={1} title={"Jessica Neil"} />
    </div>
  );
};
