import * as React from "react";

type IIconsProps = {
  Icon: any;
  className?: string;
  style?: string;
};

export const Icons: React.FunctionComponent<IIconsProps> = ({
  Icon,
  className,
  style,
}) => {
  return (
    <div
      className={`${style} flex curser-pointer items-center    md:hover:bg-gray-100 rounded-xl `}
    >
      <Icon className={className} />
    </div>
  );
};
