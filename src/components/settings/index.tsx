import React, { useState } from "react";
import { AccountSection } from "./accountSection";
import { EmailSection } from "./emailSection";
import { PasswordSection } from "./passwordSection";
import { SponsorSection } from "./sponsorSection";
import { TermsSection } from "./termsSection";
import { SettingsSection } from "../../enums/settings";
import { DoctorsMain } from "@components";
interface Props {
  name?: string;
  style?: string;
}

export const Index: React.FC<Props> = (props: Props) => {
  const [chosenSection, setChosenSection] = useState(
    SettingsSection.EmailSection
  );

  const [editAccountVisibilty, setEditAccountVisibility] = useState(false);
  const [editPasswordVisibilty, setEditPasswordVisibility] = useState(false);
  const [editEmailVisibilty, setEditEmailVisibility] = useState(false);
  const [editTermsVisibilty, setEditTermsVisibility] = useState(false);
  const [editSponsorVisibilty, setEditSponsorVisibility] = useState(false);

  const callbackFunctionAccount = async (childata: any) => {
    setEditAccountVisibility(childata);
  };

  const callbackFunctionPassword = async (childata: any) => {
    setEditPasswordVisibility(childata);
  };

  const callbackFunctionEmail = async (childata: any) => {
    setEditEmailVisibility(childata);
  };

  const callbackFunctionTerms = async (childata: any) => {
    setEditTermsVisibility(childata);
  };

  const callbackFunctionSponsor = async (childata: any) => {
    setEditSponsorVisibility(childata);
  };

  const Active: React.FC<Props> = ({ name, style }) => {
    return (
      <div className="flex flex-col h-10 justify-center  w-full text-primary-white  bg-secondary-cyan">
        <label className={`${style}`}>{name}</label>
      </div>
    );
  };

  const DeActive: React.FC<Props> = ({ name, style }) => {
    return (
      <div className="flex flex-col h-10 justify-center text-primary-black w-full  ">
        <label className={`${style}`}>{name}</label>
      </div>
    );
  };

  return (
    <div className="flex  space-y-3  relative  shadow-lg w-full  rounded-2xl  bg-primary-white">
      {editAccountVisibilty && (
        <div>
          <AccountSection
            callback={callbackFunctionAccount}
            style="flex lg:hidden absolute"
          />
        </div>
      )}

      {editPasswordVisibilty && (
        <div>
          <PasswordSection
            callback={callbackFunctionPassword}
            style="flex lg:hidden absolute"
          />
        </div>
      )}

      {editEmailVisibilty && (
        <div>
          <EmailSection
            callback={callbackFunctionEmail}
            style="flex lg:hidden absolute"
          />
        </div>
      )}

      {editTermsVisibilty && (
        <div>
          <TermsSection
            callback={callbackFunctionTerms}
            style="flex lg:hidden absolute"
          />
        </div>
      )}

      {editSponsorVisibilty && (
        <div>
          <SponsorSection
            callback={callbackFunctionSponsor}
            style="flex lg:hidden absolute"
          />
        </div>
      )}

      <div className="flex flex-col lg:border-r-2 z-0 border-others-brandText space-y-3 pb-5  w-full  min:w-308 lg:w-450px lg:max-w-308 relative">
        <label className="font-aller text-1375 p-4">Settings</label>

        <div
          onClick={() => {
            setChosenSection(SettingsSection.AccountSection);
            setEditAccountVisibility(true);
            setEditPasswordVisibility(false);
            setEditEmailVisibility(false);
            setEditTermsVisibility(false);
            setEditSponsorVisibility(false);
          }}
          className="font-interregular text-md w-full "
        >
          {chosenSection === SettingsSection.AccountSection ? (
            <Active name="Account" style="ml-5" />
          ) : (
            <DeActive name="Account" style="ml-5" />
          )}
        </div>

        <label className="font-interregular text-md ml-5">
          Security & Login Credentials
        </label>

        <div
          onClick={() => {
            setChosenSection(SettingsSection.PasswordSection);
            setEditAccountVisibility(false);
            setEditPasswordVisibility(true);
            setEditEmailVisibility(false);
            setEditTermsVisibility(false);
            setEditSponsorVisibility(false);
          }}
          className="font-interregular text-tiny w-full "
        >
          {chosenSection === SettingsSection.PasswordSection ? (
            <Active name="Change Password" style="ml-10" />
          ) : (
            <DeActive name="Change Password" style="ml-10" />
          )}
        </div>

        <div
          onClick={() => {
            setChosenSection(SettingsSection.EmailSection);
            setEditAccountVisibility(false);
            setEditPasswordVisibility(false);
            setEditEmailVisibility(true);
            setEditTermsVisibility(false);
            setEditSponsorVisibility(false);
          }}
          className="font-interregular text-tiny w-full "
        >
          {chosenSection === SettingsSection.EmailSection ? (
            <Active name="Change Email" style="ml-10 " />
          ) : (
            <DeActive name="Change Email" style="ml-10 " />
          )}
        </div>

        <div
          onClick={() => {
            setChosenSection(SettingsSection.TermsSection);
            setEditAccountVisibility(false);
            setEditPasswordVisibility(false);
            setEditEmailVisibility(false);
            setEditTermsVisibility(true);
            setEditSponsorVisibility(false);
          }}
          className="font-interregular text-md w-full "
        >
          {chosenSection === SettingsSection.TermsSection ? (
            <Active name="Terms of Service & Terms of Use" style="ml-5 " />
          ) : (
            <DeActive name="Terms of Service & Terms of Use" style="ml-5" />
          )}
        </div>

        <label className="font-interregular text-md ml-5">About</label>

        <div
          onClick={() => {
            setChosenSection(SettingsSection.SponsorSection);
            setEditAccountVisibility(false);
            setEditPasswordVisibility(false);
            setEditEmailVisibility(false);
            setEditTermsVisibility(false);
            setEditSponsorVisibility(true);
          }}
          className="font-interregular text-md w-full "
        >
          {chosenSection === SettingsSection.SponsorSection ? (
            <Active name="Be Sponsor" style="ml-5 " />
          ) : (
            <DeActive name="Be Sponsor" style="ml-5" />
          )}
        </div>
        <div className="">
          <label className="font-interregular text-md ml-5">Log Out</label>
        </div>
      </div>
      <div className="hidden lg:flex w-full">
        {chosenSection === SettingsSection.AccountSection && <AccountSection />}
        {chosenSection === SettingsSection.PasswordSection && (
          <PasswordSection />
        )}
        {chosenSection === SettingsSection.EmailSection && <EmailSection />}
        {chosenSection === SettingsSection.TermsSection && <TermsSection />}
        {chosenSection === SettingsSection.SponsorSection && <SponsorSection />}
      </div>
    </div>
  );
};

/* {chosenSection === SettingsSection.AccountSection && (
          <div className="flex lg:hidden">
            <AccountSection />
          </div>
        )}

        {chosenSection === SettingsSection.PasswordSection && (
          <div className="flex lg:hidden">
            <PasswordSection />
          </div>
        )}

        {chosenSection === SettingsSection.EmailSection && (
          <div className="flex lg:hidden">
            <EmailSection />
          </div>
        )}

        {chosenSection === SettingsSection.TermsSection && (
          <div className="flex lg:hidden">
            <TermsSection />
          </div>
        )}

        {chosenSection === SettingsSection.SponsorSection && (
          <div className="flex lg:hidden">
            <SponsorSection />
          </div>
        )} */
