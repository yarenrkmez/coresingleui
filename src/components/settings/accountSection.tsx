import React from "react";
import { Input, ButtonPrimary } from "@components";
import { UilAngleLeft } from "@iconscout/react-unicons";
import { Icons } from "@components/icons";
type Props = {
  callback?: any;
  style?: string;
};

export const AccountSection: React.FC<Props> = ({ callback, style }) => {
  return (
    <div
      className={`${style} z-10 h-full flex-col w-full rounded-xl bg-primary-white`}
    >
      <div className="flex border-b-2 items-center p-6  border-primary-twgray pel">
        <div className="flex lg:hidden">
          <button onClick={() => callback(false)}>
            <Icons Icon={UilAngleLeft} className="text-secondary-cyan" />
          </button>
        </div>
        <label className="font-inter text-md">Account</label>
        <label className="font-interregular text-primary-twgray text-buttontext ml-2">
          Change account details
        </label>
      </div>

      <div className="flex flex-col w-full p-4 space-y-3 ">
        <div className="flex flex-col w-full ">
          <label className="inputlabel">Name</label>
          <Input Placeholder="John" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">Surname</label>
          <Input Placeholder="John" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">Bio</label>
          <Input Placeholder="asdadasdad" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">Phone</label>
          <Input Placeholder="123123123" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">City</label>
          <Input Placeholder="Edirne" />
        </div>

        <div className="flex justify-end items-center ">
          <ButtonPrimary
            title="Save Changes"
            style="bg-secondary-cyan w-120px font-inter  text-buttontext p-2 text-primary-white"
          />
        </div>
      </div>
    </div>
  );
};
