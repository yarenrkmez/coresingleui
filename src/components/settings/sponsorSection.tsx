import React from "react";
import { Input, ButtonPrimary } from "@components";
import { UilAngleLeft } from "@iconscout/react-unicons";
import { Icons } from "@components/icons";
type Props = {
  callback?: any;
  style?: string;
};

export const SponsorSection: React.FC<Props> = ({ callback, style }) => {
  return (
    <div
      className={`${style} z-10 h-full flex-col w-full rounded-xl bg-primary-white`}
    >
      <div className="flex border-b-2 border-primary-twgray p-5 justify-start items-center">
        <div className="flex lg:hidden">
          <button onClick={() => callback(false)}>
            <Icons Icon={UilAngleLeft} className="text-secondary-cyan" />
          </button>
        </div>
        <div className="flex w-1/3 lg:w-1/4">
          <label className="font-inter text-md">Be Sponsor</label>
        </div>
        <div className="flex w-1/2">
          <label className="font-interregular text-primary-twgray text-buttontext ml-2">
            Sed in amet leo nisl porta vitae ullamcorper posuere. Facilisi
            blandit turpis turpis sitla.
          </label>
        </div>
      </div>

      <div className="flex flex-col w-full p-4 space-y-3 ">
        <div className="flex flex-col w-full ">
          <label className="inputlabel">Current Email</label>
          <Input Placeholder="john@doe.com" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">New Email</label>
          <Input Placeholder="helloJohn@doe.com" />
        </div>

        <div className="flex flex-col w-full">
          <label className="inputlabel">Confirm Password</label>
          <Input Placeholder="********" />
        </div>

        <div className="flex justify-end items-center ">
          <ButtonPrimary
            title="Save Changes"
            style="bg-secondary-cyan w-120px font-inter  text-buttontext p-2 text-primary-white"
          />
        </div>
      </div>
    </div>
  );
};
