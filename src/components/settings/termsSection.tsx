import React from "react";
import { UilAngleLeft } from "@iconscout/react-unicons";
import { Icons } from "@components/icons";
type Props = {
  callback?: any;
  style?: string;
};

export const TermsSection: React.FC<Props> = ({ callback, style }) => {
  return (
    <div
      className={`${style} z-10 flex flex-col w-full rounded-xl bg-primary-white`}
    >
      <div className="flex border-b-2 border-primary-twgray p-6 items-center">
        <div className="flex lg:hidden">
          <button onClick={() => callback(false)}>
            <Icons Icon={UilAngleLeft} className="text-secondary-cyan" />
          </button>
        </div>
        <label className="font-inter text-md">
          Terms of Service & Terms of Use
        </label>
      </div>

      <div className="flex flex-col w-full p-4 space-y-3 h-full overflow-y-auto ">
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </p>

        <p>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Posuere nec,
          donec at feugiat blandit sollicitudin eget. At pellentesque volutpat
          dictum nisl, consectetur sit dui rutrum. Ac ut aliquam nibh pharetra,
          nunc, consequat lectus morbi volutpat.
        </p>
        <p>
          Pharetra condimentum nisl, pellentesque diam rutrum diam placerat.
          Fermentum velit elementum cursus praesent ipsum, mauris. Blandit
          mauris tristique in nunc. Pharetra condimentum nisl, pellentesque diam
          rutrum diam placerat. Fermentum velit elementum cursus praesent ipsum,
          mauris. Blandit mauris tristique in nunc. Vel at aliquam morbi at.
          Lacus turpis sagittis, fames pulvinar. Purus enim nam enim amet nunc.
        </p>
      </div>
    </div>
  );
};
