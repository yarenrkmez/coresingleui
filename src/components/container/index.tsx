import * as React from "react";
import {
  Sidebar,
  Navbar,
  DiscoverMain,
  RoomsMain,
  DoctorsMain,
  Filters,
} from "@components";

interface IContainerProps {
  children?: any;
  style?: string;
}
interface MainContainerProps {
  children?: any;
  rightstyle?: string;
  leftstyle?: string;
  leftroomstyle?: string;
  navigatorSideStatus?: boolean;
  roomsSideStatus?: boolean;
  discoverSideStatus?: boolean;
  doctorsSideStatus?: boolean;
  filtersSideStatus?: boolean;
  childrenStyle?: string;
}

export const ContainerWhite: React.FC<IContainerProps> = ({
  children,
  style,
}) => {
  return (
    <div
      className={`${style} flex flex-col flex-start z-10     items-center  w-full md:w-800px lg:w-960px  bg-primary-white `}
    >
      {children}
    </div>
  );
};

export const ContainerBlue: React.FC<IContainerProps> = ({
  children,
  style,
}) => {
  return (
    <div
      className={`${style}  hidden w-full    md:flex flex-col justify-center items-center `}
    >
      {children}
    </div>
  );
};

export const ContainerForm: React.FC<IContainerProps> = ({
  children,
  style,
}) => {
  return (
    <div
      className={`${style} xs:p-4 z-10  sm:p-8 md:p-6 p-6  w-280px  sm:w-520px md:w-394px lg:w-450px lg:p-18 bg-white space-y-3  flex  flex-col  "`}
    >
      {children}
    </div>
  );
};

export const ContainerSidebar: React.FC<IContainerProps> = ({ children }) => {
  return (
    <div className=" flex mt-5 w-full hover:text-blue-400">{children}</div>
  );
};

export const ContainerRooms: React.FC<IContainerProps> = ({ children }) => {
  return (
    <div className=" flex    justify-between items-center ">{children}</div>
  );
};

export const ContainerPostPopup: React.FC<IContainerProps> = ({ children }) => {
  return (
    <div className=" flex w-full min-w-full  items-center p-4 ">{children}</div>
  );
};

//without page body only nav,side bars
export const ContainerMainPage: React.FC<MainContainerProps> = ({
  children,
  rightstyle,
  leftstyle,
  leftroomstyle,
  discoverSideStatus = true,
  doctorsSideStatus = true,
  navigatorSideStatus = true,
  roomsSideStatus = true,
  filtersSideStatus = true,
  childrenStyle,
}) => {
  let topDivStyle =
    "flex   justify-between w-full  sm:space-x-3 max-w-1280 mt-3";
  if (!filtersSideStatus && !discoverSideStatus && !doctorsSideStatus)
    topDivStyle = "flex  justify-between w-full max-w-1280 mt-3";
  return (
    <div className=" flex flex-col items-center p-6 mb-2">
      <Navbar
        user={{
          name: "Matehand",
          surname: "Mate",
          userName: "Matehand",
          userImage: "",
        }}
      />

      <div className={topDivStyle}>
        <div className={`${leftstyle} flex-col  max-w-308 `}>
          {navigatorSideStatus && (
            <Sidebar style="xlg:w-308px lg:w-258 max-w-308" />
          )}
          {roomsSideStatus && (
            <RoomsMain
              style={`${leftroomstyle} xlg:w-308px lg:w-258 max-w-308`}
              buttonstyle="hidden xlg:flex"
            />
          )}
        </div>

        <div
          className={`overflow-y-auto   h-160vh  w-full flex flex-col  ${childrenStyle} `}
        >
          {children}
        </div>

        <div className={`${rightstyle}  flex-col  max-w-308  mb-3 `}>
          {filtersSideStatus && (
            <Filters style="xlg:w-308px lg:w-258 max-w-308" />
          )}
          {discoverSideStatus && (
            <DiscoverMain style="xlg:w-308px lg:w-258 max-w-308" />
          )}
          {doctorsSideStatus && (
            <DoctorsMain
              style="xlg:w-308px lg:w-258 max-w-308"
              buttonstyle="hidden xlg:flex"
              usertextstyle="hidden sm:flex sm:flex-col "
            />
          )}
        </div>
      </div>
    </div>
  );
};
