import React from "react";
import Item from "./item";
import { UilAngleRight } from "@iconscout/react-unicons";
import { ButtonLink } from "@components";
type Props = {
  style?: String;
  buttonstyle?: string;
  usertextstyle?: string;
};

export const Index: React.FC<Props> = ({
  style,
  buttonstyle,
  usertextstyle,
}) => {
  return (
    <div
      className={`${style} flex flex-col relative flex-start space-y-3  md:min-w-208   shadow-lg  mb-3  rounded-2xl p-4  bg-primary-white`}
    >
      <div className="font-aller text-1375">Doctors</div>
      <Item
        buttonStatus={1}
        title={"Jessica Neil"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />
      <Item
        buttonStatus={1}
        title={"Adis Coco"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />
      <Item
        buttonStatus={2}
        title={"Jessica Neil"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />
      <Item
        buttonStatus={1}
        title={"Jessica Neil"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />
      <Item
        buttonStatus={2}
        title={"Adis Coco"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />
      <Item
        buttonStatus={1}
        title={"Jessica Neil"}
        buttonstyle={`${buttonstyle}`}
        usertextstyle={`${usertextstyle}`}
      />

      <ButtonLink
        Icon={UilAngleRight}
        style="p-2 text-primary-twgray hidden md:flex text-sm font-interregular"
        title="See More"
        IconStyle=" font-light text-center"
      />
    </div>
  );
};
