import React from "react";
import { UserCard, ButtonSecondary } from "@components";
import { ItemButtonStatus } from "../../enums/rooms";
type Props = {
  buttonStatus?: number;
  title?: string;
  buttonstyle?: string;
  usertextstyle?: string;
};

const Item: React.FC<Props> = ({
  buttonStatus,
  title,
  buttonstyle,
  usertextstyle,
}) => {
  return (
    <div className="flex justify-between items-center">
      <UserCard
        user={{
          name: `${title}`,
          surname: "Matehand",
          userName: "Matehand",
          userImage: "",
        }}
        usertextstyle={`${usertextstyle}`}
      />

      {buttonStatus === ItemButtonStatus.FollowButton && (
        <ButtonSecondary
          title="Follow"
          style={`${buttonstyle} text-secondary-cyan justify-center items-center w-70px  p-1 text-buttontext  font-inter`}
        />
      )}
      {buttonStatus === ItemButtonStatus.UnFollowButton && (
        <ButtonSecondary
          title="Unfollow"
          style={`${buttonstyle} text-primary-white justify-center items-center w-70px  p-1 text-buttontext  font-inter bg-secondary-cyan`}
        />
      )}
    </div>
  );
};

export default Item;
