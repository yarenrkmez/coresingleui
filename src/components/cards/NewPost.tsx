import { ButtonIcon } from "@components/button";
import * as React from "react";
import {
  UilFileAlt,
  UilMicrophone,
  UilImage,
  UilMessage,
} from "@iconscout/react-unicons";
type INewPostProps = {
  title?: string;
  inChat?: boolean;
};

export const NewPost: React.FunctionComponent<INewPostProps> = ({
  title,
  inChat = true,
}) => {
  let chatBorder = "";
  if (inChat)
    chatBorder =
      "bg-white flex justify-between min-w-min  items-center border-l-0 rounded-l-none border-t rounded-t-none shadow-md rounded-xl border-others-softText bg-primary-white space-x-2 min-w-min max-w-1280 lg-Max:border-primary-white";
  else
    chatBorder =
      "bg-white flex justify-between min-w-min  items-center  shadow-md  rounded-xl bg-primary-white w-full min-w-min ";
  return (
    <div className={chatBorder}>
      <input
        className="overflow-ellipsis m-5 bg-transparent h-10 outline-none  "
        placeholder={title}
      />

      <div className="flex p-3 ">
        <ButtonIcon
          Icon={UilFileAlt}
          style="text-primary-twgray hidden lg:flex"
        />
        <ButtonIcon
          Icon={UilMicrophone}
          style="text-primary-twgray hidden md:flex"
        />
        <ButtonIcon
          Icon={UilImage}
          style="text-primary-twgray hidden md:flex"
        />
        {inChat && <div className="border border-others-twGray" />}

        <ButtonIcon Icon={UilMessage} style="text-secondary-cyan" />
      </div>
    </div>
  );
};
