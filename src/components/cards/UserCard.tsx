import React, { useState } from "react";
import { Profile, SearchResult, Message, Messages } from "@components";
import { UilBell, UilCommentAltMessage } from "@iconscout/react-unicons";

interface Props {
  user?: {
    name: string;
    surname: string;
    userName: string;
    userImage?: string;
  };
  isIcon?: boolean;
  style?: string;
  usertextstyle?: string;
}
export const UserCard: React.FC<Props> = ({
  user,
  isIcon,
  style,
  usertextstyle,
}) => {
  const [editProfileVisibilty, setEditProfileVisibility] = useState(false);
  const [editMessageVisibilty, setEditMessageVisibility] = useState(false);
  const [editSearchVisibilty, setEditSearchVisibility] = useState(false);

  const callbackFunctionProfile = async (childata: any) => {
    setEditProfileVisibility(childata);
  };

  const callbackFunctionMessage = (childata: any) => {
    setEditMessageVisibility(childata);
  };

  const callbackFunctionSearch = (childata: any) => {
    setEditSearchVisibility(childata);
  };

  return (
    <div className={`flex items-center max-w-xs space-x-3 ${style}`}>
      {editProfileVisibilty && (
        <div>
          <Profile callback={callbackFunctionProfile} />
        </div>
      )}

      {editMessageVisibilty && (
        <div>
          <Message callback={callbackFunctionMessage} />
        </div>
      )}

      {editSearchVisibilty && (
        <div>
          <SearchResult callback={callbackFunctionSearch} />
        </div>
      )}

      {isIcon && (
        <>
          <div
            className="flex flex-col-reverse text-1375 "
            onClick={() => {
              setEditMessageVisibility(true);
              setEditProfileVisibility(false);
              setEditSearchVisibility(false);
            }}
          >
            <div className="bg-successError-darkError rounded-full h-2 w-2  self-end absolute mb-4  border border-primary-white "></div>
            <UilCommentAltMessage />
          </div>

          <div
            className="flex flex-col-reverse  text-1375 "
            onClick={() => {
              setEditProfileVisibility(false);
              setEditMessageVisibility(false);
              setEditSearchVisibility(true);
            }}
          >
            <div className="bg-successError-darkError rounded-full h-2 w-2  self-end absolute mb-4 mr-1 border border-primary-white"></div>
            <UilBell />
          </div>
        </>
      )}

      <div
        className="flex  "
        onClick={() => {
          setEditProfileVisibility(true);
          setEditMessageVisibility(false);
          setEditSearchVisibility(false);
        }}
      >
        <div className="z-0">
          <div className="flex z-10 userImage absolute object-cover  ">
            <img
              src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
              alt="user"
            />
          </div>

          <svg
            width="50"
            height="44"
            viewBox="0 0 50 44"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              d="M2.01036 19.25C1.02788 20.9517 1.02788 23.0483 2.01036 24.75L11.1236 40.5346C12.1061 42.2363 13.9218 43.2846 15.8868 43.2846H34.1132C36.0782 43.2846 37.8939 42.2363 38.8764 40.5346L47.9896 24.75C48.9721 23.0483 48.9721 20.9517 47.9896 19.25L38.8764 3.46539C37.8939 1.76369 36.0782 0.715393 34.1133 0.715393H15.8868C13.9218 0.715393 12.1061 1.76369 11.1236 3.46539L2.01036 19.25Z"
              stroke="#53E0DB"
            />
          </svg>
        </div>
      </div>

      <div className={`${usertextstyle} ml-2 flex-col`}>
        <p className="font-inter ">{user?.name}</p>
        <p className=" baselabel text-primary-twgray  font-interregular">
          {user?.userName}
        </p>
      </div>
    </div>
  );
};
