import * as React from "react";
import { UilSearch } from "@iconscout/react-unicons";
type ISearchBarProps = {
  style?: string;
  placeholder?: string;
  iconStyle?: string;
  placeholderStyle?: string;
};
export const SearchBar: React.FunctionComponent<ISearchBarProps> = ({
  style = "border-black border  bg-gray-100",
  placeholder = "Search",
  iconStyle,
  placeholderStyle,
}) => {
  return (
    <div
      className={`flex-inline border-none md:border-solid flex  items-center justify-center p-2 rounded-full ${style}`}
    >
      <UilSearch className={iconStyle} />
      <div className="hidden md:flex ">
        <input
          className={`ml-2 items-center  outline-none ${placeholderStyle} `}
          placeholder={placeholder}
        />
      </div>
    </div>
  );
};
