import * as React from "react";
import {
  UilUserCheck,
  UilApps,
  UilExclamationOctagon,
} from "@iconscout/react-unicons";
import { ContainerPostPopup, Icons, PostPopupLabel } from "@components";

export const PostPopup: React.FunctionComponent = () => {
  return (
    <div className="flex flex-col min-w-min  items-center  border shadow-lg  rounded-lg  container w-1/4  my-6   p-3">
      <ContainerPostPopup>
        <Icons Icon={UilUserCheck} />
        <PostPopupLabel title="Kişiyi takip et (@neil_jesss)" />
      </ContainerPostPopup>
      <ContainerPostPopup>
        <Icons Icon={UilApps} />
        <PostPopupLabel title="Odayı takip et (Coronavirus)" />
      </ContainerPostPopup>
      <ContainerPostPopup>
        <Icons Icon={UilExclamationOctagon} />
        <PostPopupLabel title="Gönderiyi şikayet et" />
      </ContainerPostPopup>
      <ContainerPostPopup>
        <Icons Icon={UilExclamationOctagon} />
        <PostPopupLabel title="Kullanıcıyı şikayet et (@neil_jess)" />
      </ContainerPostPopup>
    </div>
  );
};
