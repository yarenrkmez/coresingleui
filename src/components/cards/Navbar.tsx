import React from "react";
import { useRouter } from "next/router";
import Image from "next/image";
import { UserCard } from "@components";
import { SearchBar } from "./SearchBar";

type Props = {
  brandIcon?: string;
  brandName?: string;
  visiblitySearch?: string;
  icons?: [];
  user?: { name: string; surname: string; userName: string; userImage: string };
};

export const Navbar: React.FC<Props> = ({ user }) => {
  const router = useRouter();

  return (
    <div className=" bg-white  self-center space-x-3 flex justify-between md:min-w-208 lg:min-w-308 w-full max-w-1280   shadow-md  rounded-xl p-3 bg-primary-white">
      <link
        rel="stylesheet"
        href="https://unicons.iconscout.com/release/v4.0.0/css/line.css"
      ></link>
      <div className="flex items-center space-x-3 ">
        <Image src="/icons/asdas.svg" width="40px" height="40px" />
        <p className="hidden sm:flex font-aller  text-others-brandText text-2xl ">
          MateHand
        </p>

        <div className="hidden sm:flex items-center">
          <SearchBar />
        </div>
      </div>

      <UserCard user={user} isIcon={true} usertextstyle="hidden md:flex" />
    </div>
  );
};
