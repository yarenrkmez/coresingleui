import React from "react";
import Image from "next/image";
import { Icons } from "@components";

import {
  UilCommentMessage,
  UilHeart,
  UilHeartBreak,
  UilRedo,
  UilEllipsisH,
} from "@iconscout/react-unicons";

interface Props {
  body?: string;
  image?: string;
}

export const Post: React.FC<Props> = ({ body, image }) => {
  return (
    <div className="flex flex-col space-y-5 items-center mb-2  shadow-md mt-2 w-full    min-w-min   rounded-xl p-6 bg-primary-white ">
      <div className="flex justify-between w-full ">
        <div className="flex flex-col">
          <div className="flex flex-row">
            <img
              src="https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg"
              alt="user"
              className="userImagePost"
            />
            <div className="flex flex-col  ">
              <div className="flex items-start justify-start">
                <label className="flex font-inter ml-2 text-md">
                  yarenilal
                  <label className="flex self-center font-interregular ml-2 text-buttontext text-others-twGray">
                    @ilal
                  </label>
                </label>
              </div>
              <div className="flex items-start justify-start">
                <label className="font-interregular text-buttontext bg-primary-gray text-others-twGray pl-1 pr-1 ml-2 rounded-full">
                  Coronavirus
                </label>
              </div>
            </div>
          </div>
        </div>

        <div className="flex flex-row text-center ustify-center items-center space-x-3 right-0 ">
          <label className="font-interregular text-buttontext">22m</label>
          <Icons style="text-secondary-cyan" Icon={UilEllipsisH} />
        </div>
      </div>

      <p className="font-interregular text-md">
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras sem
        aliquam eu pharetra et. Massa enim diam pulvinar sem risus est nunc.
        Scelerisque semper at sapien quis nunc sollicitudin erat ac. Diam at
        mauris suspendisse amet, sollicitudin auctor. Lorem ipsum dolor sit
        amet, consectetur adipiscing elit. Cras sem aliquam eu pharetra et.
      </p>
      {/* <div className="h-128 w-144  bg-contain bg-center border border-r-3 overflow-hidden" style={{
                backgroundImage: "url(" + "https://cdn.pixabay.com/photo/2020/10/05/10/51/cat-5628953_960_720.jpg" + ")",
                backgroundPosition: 'center',
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                overflow: 'hidden',
                borderRadius: '20px',
                borderWidth: '0px'
            }}   >

            </div> */}

      <div className="flex justify-between  w-full">
        <div className="flex flex-row items-center space-x-1">
          <Icons Icon={UilHeart} />
          <label className="font-interregular text-buttontext">135</label>
        </div>
        <div className="flex flex-row items-center space-x-1">
          <Icons Icon={UilHeartBreak} />
          <label className="font-interregular text-buttontext">1</label>
        </div>
        <div className="flex flex-row items-center space-x-1">
          <Icons Icon={UilCommentMessage} />
          <label className="font-interregular text-buttontext">41</label>
        </div>
        <div className="flex flex-row items-center space-x-1">
          <Icons Icon={UilRedo} />
          <label className="font-interregular text-buttontext">22</label>
        </div>
      </div>
    </div>
  );
};
