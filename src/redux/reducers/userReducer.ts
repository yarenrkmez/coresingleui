import {userActions} from '../actionTypes';

const initialState = {
  user: {},
};

const storiesReducer = (state = initialState, action?:any) => {
  switch (action.type) {
    case userActions.SET_USER_DATA:
      return {
        ...state,
        user: action.data,
      };

    default:
      return state;
  }
};

export default storiesReducer;
