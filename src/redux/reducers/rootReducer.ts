import {combineReducers} from 'redux';
import userReducer from './userReducer';


const allReducers = {
userReducer
};

const rootReducer = combineReducers(allReducers);
export type RootState = ReturnType<typeof rootReducer>
export default rootReducer;
