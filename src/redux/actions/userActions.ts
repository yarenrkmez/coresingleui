import {userActions} from '../actionTypes';

export const setUserSession = (data?:any) => ({
  type: userActions.SET_USER_DATA,
  data,
});
