export enum SettingsSection {
  AccountSection = 1,
  PasswordSection = 2,
  EmailSection = 3,
  TermsSection = 4,
  SponsorSection = 5,
}
