export enum ProfileSection {
    PostsSection = 1,
    CommentsSection = 2,
    LikesSection = 3,
    SharesSection = 4,
  }
  