export enum NotifItemButtonStatus {
    FollowButton = 1,
    NoButton = 2,
    UnFollowButton = 3,
}